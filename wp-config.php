<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$:!<O-hD#-e5+$@~~&F/Q%`z:+!(+l%b6UWK#R,M9n3^7DOD}yf$0`sF*?lUiLvM');
define('SECURE_AUTH_KEY',  '|F|&k3#6:q5UWyE$gUBun9,dO+tes8zaFJ{y|sFmu H!72/#m7Y17VzH;^b_Byfp');
define('LOGGED_IN_KEY',    '=]6@htF#@84Dw]od*,^ZE<jf BT0Zas+y?C|]YN_gdWVmrUp*:r6m9oM{fo.r;LG');
define('NONCE_KEY',        'y&);An+^TT+T:%i<_p|x07 r?hnzZ]^~F/dsxHGS~9.@o-L-pWDH^aLg1<lt5k>Q');
define('AUTH_SALT',        'TjzvFj_{ad^)fp-%Af*+`.h-_1Ko(Qw5!f5XUpUUL6&tL#BDB#>O=Ps&4w-HD-sS');
define('SECURE_AUTH_SALT', 'bK8Geqfgp8R4@-cDG*#FeoIPFxf(%U{Uv!+w0?V$tX)fHFvv1}4<BdoE1LJg|)4P');
define('LOGGED_IN_SALT',   '8`1W8odsMBqEMtp}!NXR?JPx]#Et[&TBbe3JQU,oe%e!6+X_l=@6l6+|RWneI:w?');
define('NONCE_SALT',       ' 0@{=pP#l(C~aA^wK60O<K3oFVoa&)3,Zn^TG`g&S3/kW)4;Ki|;Ep>_-}r#6>*v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
