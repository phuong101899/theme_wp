<div class="before-footer">
	<div class="row">
		<div class="col-md-4 section-item">
			<div class="media">
				<div class="media-left media-middle">
					<img class="icon" src="images/icons/icon_location.svg">
				</div>
				<a href="#" class="media-body media-middle">
					<h3 class="media-heading">Seldat Worldwide</h3>
				</a>
			</div>
		</div>
		<div class="col-md-4 section-item">
			<div class="media">
				<div class="media-left media-middle">
					<img class="icon" src="images/icons/icon_download.svg">
				</div>
				<a class="media-body media-middle" target="_blank" href="/files/SELDAT_BOOKLET.pdf">
					<h4 class="media-heading">Download</h4>
					<h3 class="media-heading">Seldat Brochure</h3>
				</a>
			</div>
		</div>
		<div class="col-md-4 section-item">
			<div class="media">
				<div class="media-left media-middle">
					<img class="icon" src="images/icons/icon_customer.svg">
				</div>
				<a class="media-body media-middle" href="#">
					<h3 class="media-heading">Career Opportunities</h3>
				</a>
			</div>
		</div>
	</div>
</div>