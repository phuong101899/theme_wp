<?php

function create_post_type_careers()
{
    register_post_type('careers',
        array(
            'labels' => array(
                'name' => __('Careers'),
                'singular_name' => __('Careers'),
                'add_new' => __('Add New'),
                'add_new_item' => __('New career'),
                'edit' => __('Edit'),
                'edit_item' => __('Edit career'),
                'new_item' => __('New career item'),
                'view' => __('View HTML5 Blank Custom Post'),
                'view_item' => __('View HTML5 Blank Custom Post'),
                'search_items' => __('Search'),
                'not_found' => __('No result'),
                'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash')
            ),
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'menu_icon'     => 'dashicons-admin-users',
            'supports' => [],
            'rewrite'   => [
                'slug'  => 'careers'
            ]
        ));
}
add_action('init', 'create_post_type_careers');



add_filter( 'rwmb_meta_boxes', 'careers_prefix_meta_boxes' );
function careers_prefix_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'title'      => __( 'Careers advance'),
        'post_types' => 'careers',
        'fields'     => array(
            [
                'id'   => 'careers-quantity',
                'name' => __( 'Quantity'),
                'type' => 'number',
                'std'  => '1',
                'attributes'    => [
                    'min'   => 0
                ]
            ],
            [
                'id'   => 'careers-deadline',
                'name' => __( 'Deadline'),
                'type' => 'date',
            ],
            [
                'id'   => 'careers-location',
                'name' => __( 'Location'),
                'type' => 'post',
                'post_type' => 'location',
                'field_type'  => 'checkbox_list'
            ]
        ),
    );
    return $meta_boxes;
}
