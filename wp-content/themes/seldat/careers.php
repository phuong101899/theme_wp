<?php

$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
$careers = new WP_Query(array('post_status'=>'publish','post_type'=>'careers','paged'=>$paged));
?>


<?php if($careers->have_posts()): ?>

<?php foreach ($careers->get_posts() as $key=>$item): ?>

<?php $meta = get_post_meta( $item->ID); ?>
<div class="panel panel-default-outline">
    <div class="panel-heading" role="tab" data-toggle="collapse" data-target="#<?php echo $item->ID ?>">
        <div class="panel-title">
            <a role="button"> <?php echo $item->post_title ?> </a>
            <span class="panel-title-sub">
                <?php
                    $sub = [];
                    if($meta['careers-quantity'][0])
                        $sub[] = 'Quantity : '. $meta['careers-quantity'][0];
                    if($meta['careers-deadline'][0])
                        $sub[] = 'Deadline : '.$meta['careers-deadline'][0];
                    $sub_text = implode(' - ',$sub);
                    echo $sub_text;
                ?>
            </span>
        </div>

        <div class="panel-toolbar">
            <a role="button" data-toggle="collapse" href="#<?php echo $item->ID ?>"
               class="collapsed"> <i class="toggle-icon"></i>
            </a>
        </div>
    </div>
    <div id="<?php echo $item->ID ?>" class="panel-collapse collapse"
         role="tabpanel">
        <div class="panel-body content-line">
            <?php echo $item->post_content ?>
        </div>
        <div class="panel-footer">
            <form enctype="multipart/form-data" method="post"
                  action="<?php echo esc_url( admin_url('admin-post.php') ); ?>#hash-alert-email">
                <div class="row no-padding">
                    <div class="col-md-12">
                        <h4 class="common-title">Submit resume</h4>
                    </div>
                </div>
                <div class="row" style="padding: 0">
                    <div class="col-md-6">
                        <div class="form-group">

                            <input type="email" class="form-control" placeholder="Email"
                                   name="email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address"
                                   name="address">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone"
                                   name="phone" >
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly="readonly"
                                       placeholder="CV" required> <span class="input-group-btn"> <label
                                        class="btn btn-default btn-file"> Browse <input type="file"
                                                                                        name="attachment" style="display: none;"
                                                                                        accept="application/pdf">
												</label>
												</span>
                            </div>
                        </div>

                        <input type="hidden" name="subject" value="<?php echo $item->post_title ?>">
                        <input type="hidden" name="title" value="New Candidate">
                        <input type="hidden" name="action" value="contact_form">

                        <input type="hidden" name="mail" value="<?php global $seldat_options; echo $seldat_options['mail-career'] ?>"/>

                        <button class="btn btn-primary btn-effect pull-right" >Send</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<?php endforeach; ?>
<?php get_template_part('pagination') ?>
<?php endif; ?>

<?php wp_reset_query(); ?>
