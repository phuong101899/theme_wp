
    <h4 class="common-title">Job Description</h4>
    <p>Are you a highly motivated, energetic, customer service superstar? Do you love engaging with others and get a kick out of delivering

        excellent customer service solutions? Our company, a rapidly growing, international logistics organization is seeking a number of

        Customer Service representatives to provide excellent account management and customer service to our existing and new

        customers. Not only will you provide customer service but will also be responsible for cross-selling and assisting in growing the

        business.

        This is a great opportunity to secure full time work in an organization which offers a range of promotional opportunities for the best

        talent. Further enhance your career as a customer service professional with us today!</p>

    <h4 class="common-title">Your responsibilities</h4>
    <ul class="list-arrow-right">
        <li>Keep each client updated regarding matters such as orders, receiving, inventory, adjustments, schedule changes</li>
        <li>Respond to emails and phone calls from clients and relay important information</li>
        <li>Account management</li>
        <li>Data entry</li>
        <li>Administration</li>
        <li>Communicate with clients and resolve issues as needed</li>
        <li>Give informative answers to all questions in a friendly manner</li>
        <li>Go above and beyond in service to clients</li>
        <li>Help build a customer base</li>
        <li>Attend staff meetings and/or meetings with clients as required</li>
        <li>Effectively up-sell whenever possible</li>
        <li>Perform other tasks and special projects as assigned</li>
    </ul>

    <h4 class="common-title">What you’ll need</h4>
    <ul class="list-arrow-right">
        <li>Demonstrated customer service experience, preferably within a call centre or warehousing environment</li>
        <li>Previous administrative experience, as you will be working in a fast-paced, high-volume environment</li>
        <li>The ability to build effective and positive relationships quickly with all customers</li>
        <li>Exceptional verbal and written communication skills</li>
        <li>Experience with administrative software including Microsoft Office and WMS software</li>
        <li>Highly motivated and enthusiastic with an outgoing personality</li>
        <li>Strong attention to detail</li>
        <li>A desire to grow with the company and a passion for excellence</li>
    </ul>
