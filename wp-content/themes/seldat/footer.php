			<button class="back-top">
				<i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
			</button>
			</div>
			<footer>
				<nav class="navbar navbar-landing">
					<div class="container-fluid">

						<ul class="nav navbar-nav">
							<li><a href="#">
									Copyright &copy; Seldatinc 2016
								</a></li>
							<!--
							<li><a>|</a></li>
							<li>
								<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

							</li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
							 -->

						</ul>
					</div><!-- /.container-fluid -->
				</nav>
			</footer>
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
