<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<base href="<?php echo get_template_directory_uri(); ?>/public/">
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/public/images/logo/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300normal,300italic,400normal,400italic,600normal,600italic,700normal,700italic,800normal,800italic|Roboto:400normal|Lato:400normal|Oswald:400normal|Open+Sans+Condensed:300normal|Raleway:400normal|Montserrat:400normal|Source+Sans+Pro:400normal|Indie+Flower:400normal|PT+Sans:400normal|Merriweather:400normal&subset=all' rel='stylesheet' type='text/css'>


		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

	<div class="wrap">
		<div id="top-header">
			<nav>
				<div class="container-fluid">
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="flex-center">
						<div class="dropdown hidden-xs">
							<?php $language_list = pll_languages_list(array( 'fields' => null ));  ?>


							<?php foreach ($language_list as $l): ?>
								<?php if($l->slug == pll_current_language()): ?>
									<a href="#" class="dropdown-toggle flex-center" data-toggle="dropdown" role="button"
									   aria-haspopup="true" aria-expanded="false">
										<?php echo $l->flag ?>
										<span>&nbsp; <?php echo $l->name ?></span>
										<span class="caret"></span>
									</a>
									<?php break; ?>
								<?php endif; ?>
							<?php endforeach; ?>




							<ul class="dropdown-menu">
								<?php pll_the_languages(['show_flags'=>1,'show_names'=>1,'hide_current'=>1]) ?>
							</ul>
						</div>


						<ul class="nav-contact">
							<li><a href="tel:+17323480000"><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> + 1-732-348-0000</a></li>

							<li><a>|</a></li>

							<li>
								<a href="mailto:info@seldatinc.com">
									<i class="glyphicon glyphicon-envelope"></i> support@seldatinc.com
								</a>
							</li>

							<!--
                            <li style="background: #012147;padding: 0 15px;margin-left: 10px">
                                <a href="http://customerportal.seldatdirect.com" target="_blank" style="color: #fff">
                                    Customer Login
                                </a>
                            </li>
                            -->

							<!--
                            <li><a>|</a></li>
                            <li>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                             -->

						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
		<div id="content-wrap">
			<div id="nav-menu">
				<nav class="navbar navbar-landing">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<?php global $seldat_options; ?>
							<?php if($seldat_options['logo-switch']): ?>
							<a href="<?php echo esc_url(home_url()) ?>">
								<?php $logo_url = $seldat_options['logo-upload']['url'] ?: get_template_directory_uri().'/public/images/logo/logoseldat.svg' ?>
								<img class="img-responsive" src="<?php echo $logo_url ?>">
							</a>
							<?php endif; ?>

							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
									data-target="#navbar-collapse-top" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<i class="glyphicon glyphicon-menu-hamburger"></i>
							</button>

						</div>

						<span class="hidden-xs" style="margin-left: auto;margin-right: auto"></span>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="navbar-collapse-top">

							<?php
							wp_nav_menu(
								array(
									'theme_location'  => 'header-menu',
									'menu_class'      => 'nav navbar-nav navbar-right nav-effect',
									'menu_id'         => 'primary-menu',
									'walker'          => new custom_nav
								)
							);
							?>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>

