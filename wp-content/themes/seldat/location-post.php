<?php

function create_post_type_location()
{
    register_post_type('location',
        array(
            'labels' => array(
                'name' => __('Location'),
                'singular_name' => __('Location'),
                'add_new' => __('Add New'),
                'add_new_item' => __('New location'),
                'edit' => __('Edit'),
                'edit_item' => __('Edit location'),
                'new_item' => __('New location item'),
                'view' => __('View HTML5 Blank Custom Post'),
                'view_item' => __('View HTML5 Blank Custom Post'),
                'search_items' => __('Search'),
                'not_found' => __('No result'),
                'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash')
            ),
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'menu_icon'     => 'dashicons-admin-site',
            'supports' => ['title'],
            'rewrite'   => [
                'slug'  => 'location'
            ]
        ));
}
add_action('init', 'create_post_type_location');



add_filter( 'rwmb_meta_boxes', 'location_prefix_meta_boxes' );
function location_prefix_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'title'      => __( 'Location information'),
        'post_types' => 'location',
        'fields'     => array(
            [
                'id'   => 'location-address',
                'name' => __( 'Location address' ),
                'type' => 'text',
            ],
            [
                'id'   => 'location-phone',
                'name' => __( 'Location phone'),
                'type' => 'text',
            ],
            [
                'id'   => 'location-mail',
                'name' => __( 'Location mail'),
                'type' => 'text',
            ]
        ),
    );

    $meta_boxes[] = array(
        'title'  => __( 'Google Map'),
        'post_types' => 'location',
        'fields' => array(
            // Map requires at least one address field (with type = text)
            array(
                'id'   => 'location-find-address',
                'name' => __( 'Find Address', 'your-prefix' ),
                'type' => 'text',
            ),
            array(
                'id'            => 'map',
                'name'          => __( 'Location', 'your-prefix' ),
                'type'          => 'map',
                'address_field' => 'location-find-address',
            ),
        ),
    );



    return $meta_boxes;
}


// add_filter("manage_edit-location_columns", "location_manager_edit_columns");

function location_manager_edit_columns($columns)
{

    $columns = [
        'location-name'  => __('Location name'),
    ];
    return $columns;
}


// add_action("manage_location_posts_custom_column", "location_manager_custom_columns");

function location_manager_custom_columns($column)
{
    global $post;
    $custom = get_post_custom();

    switch ($column)
    {
        case "location-name":
        {
            echo '<strong><a class="row-title" href="'.get_edit_post_link($post->ID,$custom["location-name"][0]).'">'.$custom["location-name"][0].'</a></strong>';
            break;
        }
    }
    return $column;
}