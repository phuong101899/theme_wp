<?php
$location = new WP_Query(array('post_status'=>'publish','post_type'=>'location'));
?>
<?php if($location->have_posts()): ?>
<div class="left-country">
    <div class="panel-group" id="location_collapse" role="tablist"
         aria-multiselectable="true">
        <?php foreach ($location->get_posts() as $key=>$item): ?>
        <?php $meta = get_post_meta( $item->ID); ?>
        <div class="panel panel-default-outline">
            <div class="panel-heading" role="tab" data-toggle="collapse" data-parent="#location_collapse" data-target="#<?php echo $item->ID ?>">
                <div class="panel-title">
                    <a role="button"> <?php echo $item->post_title ?> </a>
                </div>

                <div class="panel-toolbar">
                    <a role="button" data-toggle="collapse"
                       data-parent="#location_collapse" href="#<?php echo $item->ID ?>" class="collapsed">
                        <i class="toggle-icon"></i>
                    </a>
                </div>
            </div>
            <div id="<?php echo $item->ID ?>" class="panel-collapse collapse" data-map="<?php echo $meta['map'][0] ?>"
                 role="tabpanel">
                <div class="panel-body">
                    <div class="left-location">
                        <ul>
                            <li><span class="glyphicon glyphicon-map-marker list-style"></span>
                                <?php echo $meta['location-address'][0] ?></li>
                            <li><span class="glyphicon glyphicon-earphone list-style"></span>
                                <?php echo $meta['location-phone'][0] ?></li>
                            <li><span class="glyphicon glyphicon-envelope list-style"></span>
                                <?php echo $meta['location-mail'][0] ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

    </div>

</div>
<?php endif; ?>