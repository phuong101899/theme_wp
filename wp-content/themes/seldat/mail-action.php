<?php
if (!session_id()) @session_start();
require_once get_template_directory().'/lib_php/FlashMessages.php';

function handle_mail_action()
{


    require_once ABSPATH.WPINC.'/class-phpmailer.php';

    global $seldat_options;


    $data = get_request_data();

    $mail = new PHPMailer();

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';                       // Specify main and backup server
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $seldat_options['mail-system'];                   // SMTP username
    $mail->Password = $seldat_options['mail-system-password'];                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
    $mail->Port = 587;                                    //Set the SMTP port number - 587 for authenticated TLS
    $mail->setFrom($seldat_options['mail-system'], 'Seldat Inc');     //Set who the message is to be sent from
    //$mail->addReplyTo('labnol@gmail.com', 'First Last');  //Set an alternative reply-to address

    $mailTo = $data['mail'] ? :$seldat_options['mail-contact'];

    $mail->addAddress($mailTo);  // Add a recipient

    $files = getFiles('attachment');

    if($files)
    {
        $mail->addAttachment($files['tmp_name'],$files['name']);         // Add attachments
    }

    //$mail->addAttachment('/images/image.jpg', 'new.jpg'); // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = implode(' - ', [$data['title'],$data['subject']]);
    //$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->msgHTML(getHtml($data));

    $msg = new \Plasticbrain\FlashMessages\FlashMessages();
    if(!$mail->send())
    {
        $msg->error('Mailer Error: '.$mail->ErrorInfo);
    }

    $msg->success('Email sent successfully. Thank You for your interest in Seldat Inc.');



   if(wp_get_referer())
   {
       wp_safe_redirect( wp_get_referer() );
   }
    else
    {
        wp_safe_redirect(get_home_url());
    }
}

function get_request_data()
{
    return $_REQUEST;
}

function getHtml($data)
{
    $phone = isset($data['phone']) ? '<li>Phone : '.$data['phone'].'</li>' : '';
    $address = isset($data['address']) ? '<li>Address : '.$data['address'].'</li>' : '';

    $message = isset($data['message']) ? '<li>Message : '.$data['message'].'</li>' : '';

    $full_name = isset($data['full_name']) ? '<li>Full name : '.$data['full_name'].'</li>' : '';

    $email = isset($data['email']) ? '<li>Email : '.$data['email'].'</li>' : '';

    $sub_html = $full_name.$phone.$address.$message.$email;
    $html_tmp = '<ul style="list-style : none">'.$sub_html.'</ul>';

    return $html_tmp;
}

function showFlashMessages()
{
    $msg = new \Plasticbrain\FlashMessages\FlashMessages();

    return $msg->display();
}

function hasFlashMessages()
{
    $msg = new \Plasticbrain\FlashMessages\FlashMessages();

    return $msg->hasMessages();
}

function getFiles($name)
{
   if($_FILES[$name]['name'] !== '')
   {
       return $_FILES[$name];
   }

    return false;
}


add_action( 'admin_post_nopriv_contact_form', 'handle_mail_action' );
add_action( 'admin_post_contact_form', 'handle_mail_action' );
