<?php

function create_post_type_ourservices()
{
    register_post_type('services',
        array(
            'labels' => array(
                'name' => __('Our services config', 'seldat'), // Rename these to suit
                'singular_name' => __('Our services config', 'seldat'),
                'add_new' => __('Add New', 'seldat'),
                'add_new_item' => __('New service item', 'seldat'),
                'edit' => __('Edit', 'seldat'),
                'edit_item' => __('Edit service item', 'seldat'),
                'new_item' => __('New service item', 'seldat'),
                'view' => __('View HTML5 Blank Custom Post', 'seldat'),
                'view_item' => __('View HTML5 Blank Custom Post', 'seldat'),
                'search_items' => __('Search', 'seldat'),
                'not_found' => __('No result', 'seldat'),
                'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'seldat')
            ),
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'menu_icon'     => 'dashicons-image-flip-horizontal',
            'supports' => array(
                //'title',
                //'editor',
                //'excerpt',
                //'thumbnail',
            )
        ));
}
add_action('init', 'create_post_type_ourservices');



add_filter( 'rwmb_meta_boxes', 'ourservices_prefix_meta_boxes' );
function ourservices_prefix_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Images selection', 'seldat' ),
        'post_types' => 'services',
        'fields'     => array(
            array(
                'id'   => 'front-image',
                'name' => __( 'Front', 'seldat' ),
                'type' => 'file_input',
            ),
            [
                'id'   => 'back-image',
                'name' => __( 'Back', 'seldat' ),
                'type' => 'file_input',
            ],
            [
                'id'   => 'link-to',
                'name' => __( 'Link to page', 'seldat' ),
                'type' => 'post',
                'post_type' => 'page'
            ]
        ),
    );
    return $meta_boxes;
}