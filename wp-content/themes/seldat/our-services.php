<?php
$services = new WP_Query(array('post_status'=>'publish','post_type'=>'services'));
?>

<?php if($services->have_posts()): ?>
<section>
		<div class="landing-title">
			<span class="landing-line"></span><span>our services</span><span
				class="landing-line"></span>
		</div>
		<div id="landing-services">
			<div class="float_center">
                <?php while ($services->have_posts()):$services->the_post(); ?>
                    <?php $meta = get_post_meta( get_the_ID()); ?>
				<div class="item">
					<div class="flipper">
						<div class="front">
							<div class="item-header">
								<div class="item-img">
									<img class="img-circle" src="<?php echo $meta['front-image'][0] ?>"
										 alt="<?php the_title() ?>">
								</div>
							</div>
							<div class="item-content">
								<h4 class="item-title-text"><?php the_title() ?></h4>
								<div class="item-divider"></div>
								<div class="item-content-detail">
                                    <?php the_content() ?>
								</div>
							</div>
						</div>
						<div class="back">
							<div class="item-header">
								<div class="item-img">
									<img class="img-circle" src="<?php echo $meta['back-image'][0] ?>"
										 alt="<?php echo the_title() ?>">
								</div>
							</div>
							<div class="item-content">
								<div class="content-wrap">
									<h4 class="item-title-text"><?php echo the_title() ?></h4>
									<div class="item-content-readmore">
										<a class="btn btn-default item-link" href="<?php echo esc_url( get_permalink($meta['link-to'][0]) ); ?>">
											<span >Read more</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                <?php endwhile; ?>
			</div>

			<div class="clear"></div>
		</div>
</section>
<?php endif; ?>