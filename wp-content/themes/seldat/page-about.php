<?php
/*
Template Name: Template about us
*/
get_header();

?>

<style>

    .owl-item{
        text-align: center;
    }

    .owl-item img{
        display: inline-block;
        margin: 10px;
        text-align: center;
        border: 1px solid #ddd;
        width: 250px;
    }

    .owl-theme .owl-controls .owl-buttons div {
        padding: 5px 9px;
    }

    .owl-theme .owl-buttons i{
        margin-top: 2px;
    }


    .owl-theme .owl-controls .owl-buttons div {
        position: absolute;
        width: 50px;
        height: 50px;
        border-radius: 0;

        -ms-transform: rotate(45deg); /* IE 9 */
        -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
        transform: rotate(45deg);
        background: none;
    }

    .owl-theme .owl-controls .owl-buttons .owl-prev{
        left: -60px;
        top: 45px;
        border-bottom: 1px solid #ddd;
        border-left: 1px solid #ddd;
    }

    .owl-theme .owl-controls .owl-buttons .owl-next{
        right: -60px;
        top: 45px;
        border-top: 1px solid #ddd;
        border-right: 1px solid #ddd;
    }
</style>

<div id="dynamic-content">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('about')  ?:"images/banner/services/banner_about_2000.jpg" ?> ') no-repeat center center">
        <div class="banner-text">
            <?php the_title(); ?>
        </div>
    </div>

    <div class="section-text">
        <div>
            <div class="wrap-image-text">
                <div class="image-column" style="padding-right : 45px">
                    <img src="images/banner/services/about_1.png" class="img-responsive">
                </div>
                <div class="text-column text-justify content-line">
                    <p class="common-color" style="font-size: 1.2em">
                        With over 10 years in the logistics industry, Seldat Inc is specialized in providing 3PL services related to order fulfillment (B2B), pick & pack, e-commerce logistics (B2C), warehousing and distribution as well as value added services.
                    </p>
                    <p>
                        As a 3PL, we partially or completely take charge of our customers logistic operations in order to provide them with the lowest possible costs as well as a superior operational efficiency.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('video'); getVideoSection('about'); ?>

    <div class="section-text">
        <div>
            <div class="wrap-image-text">

                <div class="text-column text-justify content-line" style="text-align-last: right;-moz-text-align-last:right">
                    <p>
                        <span class="common-title">Seldat Inc</span>
                        performs various secondary operations such as kit preparations, labelling, packing, etc. Seldat Inc sets itself apart from the 3PL industry by adopting a solid partnership approach.
                    </p>
                    <p class="common-color" style="font-size: 17px">
                        We work to build a confidence based relationship with each one of our customers in order to develop solutions that are perfectly adapted and profitable for both parties.
                    </p>
                </div>
                <div class="image-column" style="padding-left: 40px">
                    <div class="back-ground-image-wrap">
                        <img src="images/banner/services/about_2.png" class="image-item">
                        <div class="text-item text-justify" style="padding: 60px">
                            Our expertise combined with our great flexibility enables us to support and accelerate our partners growth, because they can spend more time focusing on their core business...
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/banner/services/about_3.jpg');"></section>


    <div class="section-text">
        <div>
            <div class="wrap-image-text">
                <div class="image-column" style="padding-right: 30px">
                    <img src="images/banner/services/about_4.png" class="img-responsive pull-right">
                </div>
                <div class="text-column content-line text-justify">
                    <p>
                        We desire to further develop our business on the 3PL logistics market by specifically targeting companies that wish to impart their order fulfillment processes. Sedat Inc possesses their own supply chain management system (LEAD), which is at the finest point of technology and permits us to offer a very high level of order fulfillment accuracy and efficiency.
                    </p>
                    <p class="common-color" style="font-size: 17px">
                        Our strong expertise in this domain allows us to distinguish ourselves from other 3PL and 4PL industries and offer our customers world class distribution centers that meet the highest standards in the industry.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/banner/services/about_5.jpg');"></section>

    <div class="section-text">
        <div>
            <div class="row">
                <div class="col-md-12 flex-row-column">
                    <div class="back-ground-image-wrap">
                        <img src="images/banner/services/about_6.jpg">
                        <div class="text-item common-color text-center" >
                            <h4 style="font-weight: 900;line-height: 30px;font-size: 23px">
                                YOUR TRUST AND SATISFIER ARE OUR RESPONSIBILITY
                            </h4>
                        </div>
                    </div>
                    <div style="padding: 50px 0 50px 50px;">
                        <div class="content-list-item">
                            <p>
                                Being a leader and always looking for innovative solutions, Seldat Inc goal is to offer companies from all over the world, complete, efficient and strategic solutions, from the source to the shelf.
                            </p>
                        </div>
                        <div class="content-list-item">
                            <p>
                                Seldat Inc is at the forefront of the latest tendencies. Like our customers, we are not in the storage business, but rather providing solutions to rapidly turn around products while maintaining a minimum amount of inventory.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="landing-title">
        <span class="landing-line "></span><span>COMPANIES WE HAVE WORKED WITH</span><span
            class="landing-line"></span>
    </div>

    <?php global $seldat_options; ?>
    <?php if($seldat_options['gallery-id']): ?>
    <div class="section-text">
        <div class="row">
            <div class="col-md-12">
                <div id="#owl-demo" class="owl-carousel owl-theme">
                    <?php foreach (explode(',',$seldat_options['gallery-id']) as $id): ?>
                    <div><img src="<?php echo wp_get_attachment_image_url($id,'')  ?>" class="img-responsive" width="150" height="134"></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>



    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul class="wow" data-animation="bounceIn" data-timeout="200">
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>


<?php get_template_part('before-footer') ?>
<?php get_footer(); ?>
<script>
    $(document).ready(function() {

        $(".owl-carousel").owlCarousel({
            items : 4,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,3],
            pagination : false,
            navigation : true,
            autoPlay : true

        });

    });
</script>