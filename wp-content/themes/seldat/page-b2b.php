<?php
/*
Template Name: Template B2B
*/
get_header();

?>


<div id="dynamic-content">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('b2b') ?:"images/banner/services/banner_b2b_2000.jpg" ?> ') no-repeat center center">
        <div class="banner-text">
            <?php echo sprintf($post->post_title,'<br/>');  ?>
        </div>
    </div>

    <div class="section-text">
        <div class="wrap-image-text">
            <div class="image-column">
                <img src="images/banner/services/banner_b2b_1.jpg" class="img-responsive pull-right">
            </div>
            <div class="text-column text-justify">
                <div class="commerce-section-one content-line">
                    <span class="common-title" style="font-size: large">Seldat Direct</span>
                    has developed a unique software platform that matches suppliers with retailers who need the product sold by the supplier, with no credit or inventory risk to the supplier. All orders are placed in case minmums so the supplier does not have to break case packs to fulfill the orders.
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('video'); getVideoSection('b2b'); ?>

    <div class="section-text">
        <div class="landing-title">
            <span class="landing-line"></span><span>HOW IT WORKS</span><span class="landing-line"></span>
        </div>
        <div class="container-content-min">
            <p class="common-color fashion-first-text">
                After the supplier decides to join the Seldat Direct platform, our sales and IT team will work with the supplier to compile a style master that will be used to upload the product details to our platform.
            </p>

            <p>
                Seldat Direct will then collect product samples that will be professionally photographed using a unique 360 degree technology. These high resolution images will appear on the front end of the platform, along with product descriptions and pricing, where the retailers will place their orders.
            </p>

            <p>
                Once the product goes live, our Seldat Direct Sales team will utilize our platform to sell the suppliers product and take the product orders from the retailer. These orders are sent to the supplier, in real time through the Supplier Portal, and paid for by Seldat Direct, prior to shipment by the supplier. Seldat Direct picks up the order from the suppliers warehouse and Seldat Direct ships the orders to the retailer.
            </p>

        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/banner/services/banner_ecommerce_2.jpg');">
    </section>

    <div class="section-text">
        <div class="wrap-image-text">
            <div class="image-column">
                <img src="images/banner/services/banner_b2b_2.jpg" class="img-responsive">
            </div>
            <div class="text-column">
                <ul class="list-arrow-right list-padding-10">
                    <li>
                        Our specially designed e-commerce area will designate locations specifically designed for your product and account.
                    </li>
                    <li>
                        We strive to have your order picked, packed and shipped the same day.
                    </li>
                    <li>
                        Your inventory levels will be automatically restocked every day for the next day shipping, so optimal inventory levels are always maintained.
                    </li>
                    <li>
                        All inventory and orders are scanned and doubled checked for 100% accuracy Provide tracking of your shipments.
                    </li>
                    <li>
                        Our automated packaging process reduces costs by limiting wasted packaging materials and improving efficiencies.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul class="wow" data-animation="bounceIn" data-timeout="200">
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>

<?php get_template_part('before-footer') ?>


<?php get_footer(); ?>
