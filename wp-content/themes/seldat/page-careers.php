<?php
/*
Template Name: Template careers
*/
get_header();

?>


<div id="dynamic-content">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('careers') ?:"images/banner/services/banner_career_2000.jpg" ?> ') no-repeat center center">
        <div class="banner-text"><?php the_title() ?></div>
    </div>

    <div class="section-text">
        <?php if(hasFlashMessages()): ?>
            <div class="row" id="hash-alert-email">
                <div class="col-md-12">
                    <?php showFlashMessages(); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-3">
                <?php get_template_part('location') ?>
            </div>
            <div class="col-md-6">
                <?php get_template_part('careers') ?>


                <!--
                <nav class="common-pagination">
                    <ul class="pagination">
                        <li><a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
                        </a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
                        </a></li>
                    </ul>
                </nav>
                 -->
            </div>
            <div class="col-md-3">
                <img class="img-responsive"
                     src="images/banner/services/banner_career_right.jpg" />
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul class="wow" data-animation="bounceIn" data-timeout="200">
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>


<?php get_template_part('before-footer') ?>
<?php get_footer(); ?>

<script>
    $('.panel-collapse').on('show.bs.collapse',function () {
        $(this).closest('.panel-default-outline').addClass('in');
    });
    $('.panel-collapse').on('hide.bs.collapse',function () {
        $(this).closest('.panel-default-outline').removeClass('in');
    });

    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

        console.log(input);
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) console.log(log);
            }

        });
    });
</script>


<script src="<?php echo get_template_directory_uri(); ?>/public/js/scroll-to-alert.js"></script>
