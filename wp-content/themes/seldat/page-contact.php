<?php
/*
Template Name: Template contact
*/
get_header();

?>


<div id="dynamic-content">

    <div class="banner-static contact-banner"
         style="background: url('<?php  echo get_banner_page('contact')?:"images/banner/services/banner_contactus_2000.jpg"; ?> ') no-repeat center center">
        <div class="banner-text">
            <?php the_title(); ?>
        </div>
    </div>

    <div class="section-text" style="padding: 30px 15px">
        <div class="row">
            <div class="col-md-3">
                <?php get_template_part('location') ?>
            </div>
            <div class="col-md-9">
                <div  id="map_canvas" style="height: 640px"></div>
            </div>
        </div>
    </div>

    <div class="section-text contact-form" style="padding: 0 15px 90px">
        <form method="post"
              action="<?php echo esc_url( admin_url('admin-post.php') ); ?>#hash-alert-email">


            <div class="row">
                <div class="col-md-12">
                    <h3 class="title">
                        Send mail to us when you have question and feedback about services
                    </h3>
                </div>
            </div>

            <?php if(hasFlashMessages()): ?>
            <div class="row" id="hash-alert-email">
                <div class="col-md-12">
                    <?php showFlashMessages(); ?>
                </div>
            </div>
            <?php endif; ?>


            <div class="row">
                <div class="col-md-6" id="left-column">
                    <!--don't change id-->
                    <div class="form-group">
                        <div class="input-group">
							<span class="input-group-addon"> <i
                                    class="glyphicon glyphicon-user"></i>
							</span> <input type="text" class="form-control input-lg"
                                           placeholder="Full name" name="full_name" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
							<span class="input-group-addon"> <i
                                    class="glyphicon glyphicon-envelope"></i>
							</span> <input type="text" class="form-control input-lg"
                                           placeholder="E-mail" name="email" required="required" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
							<span class="input-group-addon"> <i
                                    class="glyphicon glyphicon-earphone"></i>
							</span> <input type="text" class="form-control input-lg"
                                           placeholder="Your Phone" name="phone" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
							<span class="input-group-addon"> <i
                                    class="glyphicon glyphicon-pencil"
                                    style="position: absolute; top: 10px; left: 10px;"></i>
							</span>
							<textarea id="contact-text-area" class="form-control"
                                      placeholder="Message" style="resize: none" name="message"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group">
							<span class="input-group-addon"> <i
                                    class="glyphicon glyphicon-flag"></i>
							</span> <input type="text" class="form-control input-lg"
                                           placeholder="Subject" name="subject" />
                        </div>
                    </div>
                </div>

                <input type="hidden" name="title" value="Customer contact">
                <input type="hidden" name="action" value="contact_form">
                <div class="col-md-6">
                    <button class="btn btn-primary btn-effect btn-lg btn-block" type="submit">
                        <img src="images/icons/icon_mail.svg" class="icon-mail">
                    </button>
                </div>
            </div>
        </form>


    </div>

    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul class="wow" data-animation="bounceIn" data-timeout="200">
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>

<?php get_template_part('before-footer') ?>


<?php get_footer(); ?>




<script src="<?php echo get_template_directory_uri(); ?>/public/js/scroll-to-alert.js"></script>


<script>

    $(document).ready(function (e) {
        $('.panel-collapse').on('show.bs.collapse',function () {
            $(this).closest('.panel-default-outline').addClass('in');

            //show map

            var location = $(this).data('map');
            if(location) initialize(location);
        });
        $('.panel-collapse').on('hide.bs.collapse',function () {
            $(this).closest('.panel-default-outline').removeClass('in');
        });


    });

</script>

<script>

    function initialize(location_data) {

        var parse_data = location_data.split(',');
        var lat = parseFloat(parse_data[0]);
        var lng = parseFloat(parse_data[1]);

        var location = new google.maps.LatLng(lat,lng);

        var mapOptions = {
            zoom: 12,
            center: location,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        var mapCanvas = document.getElementById('map_canvas');

        var map = new google.maps.Map(mapCanvas, mapOptions);

        new google.maps.Marker({
            position : location,
            map : map
        });
    }

    function init_map() {
        $('.panel-collapse:first').closest('.panel-default-outline').find('.panel-heading').trigger('click');
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrsZOsgygZi0FgfrHyTp3knWF2G8SXwVU&libraries=places&callback=init_map"
        async defer></script>


<script>
    $(document).on('focus','.form-control', function () {
        $(this).closest('.input-group').addClass('active');
    });
    $(document).on('blur','.form-control', function () {
        $(this).closest('.input-group').removeClass('active');
    });

    //Set height for textarea
    function getHeightTextArea() {
        var height = $('#left-column').outerHeight() - 16; // 16 is margin bottom
        $('#contact-text-area').css({'height' : height});
    }

    getHeightTextArea(); //init
</script>