<?php
/*
Template Name: Template Fashion
*/
get_header();

?>

<div id="dynamic-content">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('fashion')?:"images/banner/services/banner_customize_2000.jpg" ?> ') no-repeat center center">
        <div class="banner-text"><?php the_title() ?></div>
    </div>

    <div class="section-text">
        <div class="wrap-image-text">
            <div class="image-column">
                <img src="images/banner/services/manacan.png" style="float: right">
            </div>
            <div class="text-column text-justify">
                <div class="fashion-first-text">
                    Customize Fashion House will be revolutionizing the custom clothing industry. Using a unique technology, Customize Fashion House will allow everyone to afford tailor made clothing with the precise fit that is impossible to obtain from ready to wear.
                </div>

                <h3 class="fashion-title">Retail Experience</h3>
                <p>
                    Our retail locations are an oasis for our customers, allowing them to relax and enjoy their experience while selecting their new clothes.
                </p>

                <h3 class="fashion-title">Affordable</h3>
                <p>
                    We need to create an affordable comment here.
                </p>

                <h3 class="fashion-title">Timing </h3>
                <p>
                    Customize Inc, will allow for an unprecedented turn around time for our customers, that means last minute dresses and suits are never a problem, and will fit impeccably.
                </p>
            </div>
        </div>
    </div>

    <?php get_template_part('video'); getVideoSection('fashion'); ?>

    <div class="section-text">
        <div class="landing-title"
             style="padding: 0">
            <span class="landing-line "></span><span style="font-weight: 300">THE CUSTOMIZE EXPERIENCE</span><span
                class="landing-line"></span>
        </div>
        <h4 class="text-center">FOUR SIMPLE STEPS TO GIVE YOU GREAT STYLE AND FIT</h4>

        <div class="wrap-image-text">
            <div class="text-column text-right">
                <div class="content-list-item">
                    <h3 class="fashion-title-2">Select Your Styles </h3>
                    <p class="text-justify" style="-moz-text-align-last:right;text-align-last: right;">
                        Our collection of garments will include formal wear, business attire, shirts, skirts, and suits. Men and women will both have many different options to choose from. We will continuously add more choices to our collection, which will enable our customers to always have something special and new to wear.
                    </p>
                </div>

                <div class="content-list-item">
                    <h3 class="fashion-title-2">Choose Your Fabric</h3>
                    <p class="text-justify" style="-moz-text-align-last:right;text-align-last: right;">
                        Our customers will have many options for colors and fabrics, allowing them to have the control over the price of their garments.
                    </p>
                </div>
                <div class="content-list-item">
                    <h3 class="fashion-title-2">Measurements</h3>
                    <p class="text-justify" style="-moz-text-align-last:right;text-align-last: right;">
                        Using our unique technology, we will take precise measurements of our customers entire body. They will then be able to visualize themselves in the garment of their choice via an avatar hologram.
                    </p>
                </div>
                <div class="content-list-item">
                    <h3 class="fashion-title-2">Enhanced Styling</h3>
                    <p class="text-justify" style="-moz-text-align-last:right;text-align-last: right;">
                        Your personal stylist will assist you in creating your one of a kind outfit by helping you select your garment details such as buttons, collars, cuffs and laces.
                    </p>
                </div>
            </div>
            <div class="image-column" style="padding-left: 40px">
                <img src="images/banner/services/tablet.png"
                     class="img-responsive">
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul class="wow" data-animation="bounceIn" data-timeout="200">
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>

<?php get_template_part('before-footer') ?>


<?php get_footer(); ?>
