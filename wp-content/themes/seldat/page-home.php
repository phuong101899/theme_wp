<?php
/*
Template Name: Template home
*/
get_header(); ?>

<?php get_template_part('slider'); ?>

<div id="dynamic-content">
	<?php get_template_part('our-services'); ?>

	<section id="about-us">
		<div class="landing-title">
			<span class="landing-line "></span><span>ABOUT US</span><span
				class="landing-line"></span>
		</div>
		<div class="container-content">
			Our expertise combined with our great flexibility enables us to support and accelerate our partners growth, because they can spend more time focusing on their core business...
			<a href="#"
			   class="read-more-a">Read more</a>
		</div>

		<?php get_template_part('video'); getVideoSection('home'); ?>
	</section>

	<section id="careers">
		<div class="landing-title wow" data-animation="flipInX"
			 data-timeout="200">
			<span class="landing-line"></span><span>SELDAT CAREERS</span><span
				class="landing-line"></span>
		</div>
		<div class="container-content">
			With over 10 years in the logistics industry, Seldat Inc is specialized in providing 3PL services related to order fulfillment (B2B), pick & pack, e-commerce logistics (B2C), warehousing and distribution as well as value added services.
		</div>
		<div align="center">
			<a href="#"
			   class="btn btn-primary btn-lg read-more btn-effect">Read more</a>
		</div>
		<div class="parallax-section"
			 style="background-image: url('<?php echo get_template_directory_uri(); ?>/public/images/parallax/mission.png'); margin-top: 50px">
			<div class="parallax-modal">
				<div class="container wow" data-animation="bounceIn">
					<div class="border-length">
						<h1>SELDAT'S MISSION IS CLEAR</h1>
						<p class="mission-text">
							Provide quality services through our experienced service team by providing technological advantages at the most competitive rates
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="worldwide" data-ibg-bg="<?php echo get_template_directory_uri(); ?>/public/images/parallax/map.png">
		<div class="main flex-center">
			<div class="flex-center">
				<div class="landing-title">
					<span class="landing-line"></span><span>SELDAT WORLDWIDE</span><span
						class="landing-line"></span>
				</div>
				<div class="container-content">
					<p>
						With over 10 years in the logistics industry, Seldat Inc is specialized in providing 3PL services related to order fulfillment (B2B), pick & pack, e-commerce logistics (B2C), warehousing and distribution as well as value added services.
					</p>

				</div>
				<div align="center">
					<a class="btn btn-primary btn-lg read-more btn-effect"
					   href="#">Read more</a>
				</div>
			</div>
		</div>

	</div>

	<section class="parallax-section"
			 style="background-image: url('<?php echo get_template_directory_uri(); ?>/public/images/parallax/parallax4.jpg');">
		<div class="parallax-modal">
			<div class="container">
				<ul>
					<li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
					<li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
					<li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
					<li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
					</li>
				</ul>
			</div>
		</div>
	</section>

	<section id="contact">
		<div class="landing-title">
			<span class="landing-line"></span><span>CONTACT US</span><span
				class="landing-line"></span>
		</div>
		<div class="container-content">
			<p>
				We'd really love to hear from you so why not drop us an email and we'll get back to you as soon as we can
			</p>
			<form method="post"
				  action="<?php echo esc_url( admin_url('admin-post.php') ); ?>#hash-alert-email">

				<?php if(hasFlashMessages()): ?>
					<div class="row" id="hash-alert-email">
						<div class="col-md-12">
							<?php showFlashMessages(); ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input class="form-control input-lg" placeholder="Full name"
								   name="full_name" required="required">
						</div>
						<div class="form-group">
							<input class="form-control input-lg" placeholder="Email"
								   name="email" type="email"
								   required="required">
						</div>
						<div class="form-group">
							<input class="form-control input-lg" placeholder="Subject"
								   name="subject" required="required">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<textarea
								style="margin-top: 0px; margin-bottom: 0px; height: 107px; resize: none;"
								class="form-control input-lg" placeholder="Message"
								name="message"></textarea>
						</div>

						<input type="hidden" name="title" value="Customer contact">
						<input type="hidden" name="action" value="contact_form">
						<div class="form-group" >
							<button class="btn btn-primary btn-block btn-effect"
									type="submit"
									style="height: 46px; margin-top: -1px; margin-left: 0">
								<!--<i class="glyphicon glyphicon-envelope"></i>-->
								<img src="<?php echo get_template_directory_uri(); ?>/public/images/icons/icon_mail.svg">
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>



<script type="text/javascript">


	jQuery(document).ready(function () {
		jQuery("#home-slider").show().revolution({
			sliderType: "standard",
			//sliderLayout: "fullwidth",
			dottedOverlay: "none",
			delay: 9000,
			navigation: {
				keyboardNavigation: "off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				arrows: {
					style: "uranus",
					enable: true,
					hide_onmobile: false,
					hide_onleave: true,
					hide_delay: 200,
					hide_delay_mobile: 1200,
					tmp: '',
					left: {
						h_align: "left",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					},
					right: {
						h_align: "right",
						v_align: "center",
						h_offset: 20,
						v_offset: 0
					}
				}
			},
			gridwidth: 1024,
			gridheight: 370,
			lazyType: "none",
			shadow: 0,
			spinner: "spinner3",
			stopLoop: "off",
			stopAfterLoops: -1,
			stopAtSlide: -1,
			shuffle: "off",
			autoHeight: "off",
			disableProgressBar: "on",
			hideThumbsOnMobile: "off",
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			debugMode: false,

		});
	});
	/*ready*/
</script>


<script>

	$(document).ready(function(){
		$(".worldwide").interactive_bg();

		$(window).resize(function() {
			$(".worldwide > .ibg-bg").css({
				width: $(window).outerWidth(),
				height: $(window).outerHeight()
			});
		});
	});



</script>


<script src="<?php echo get_template_directory_uri(); ?>/public/js/scroll-to-alert.js"></script>



<?php // get_sidebar(); ?>

<?php get_footer(); ?>
