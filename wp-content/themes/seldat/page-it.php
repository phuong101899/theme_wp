<?php
/*
Template Name: Template IT Services
*/
get_header();

?>



<div id="dynamic-content" class="it-service">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('it') ?:"images/banner/services/banner_it_2000.jpg" ?> ') no-repeat center center">
        <div class="banner-text">
            <?php the_title() ?>
        </div>
    </div>

    <div class="section-text">
        <div class="landing-title">
            <span class="landing-line "></span><span>IT DESIGN SERVICES</span><span
                class="landing-line"></span>
        </div>
        <div class="wrap-image-text" style="margin-top: 40px">
            <div class="image-column">
                <img src="images/banner/services/banner_it_3.png" class="img-responsive pull-right">
            </div>
            <div class="text-column text-justify">
                <div class="commerce-section-one content-line">
                    <p class="content-list-item" style="font-size: 18px">
                        Seldat's goal is to become a leading logistic IT service provider for logistic industry and beyond.
						<span class="common-title">
							Delivering not just robust applications, but "Grade A+" 24/7 customer support.
						</span>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <?php get_template_part('video'); getVideoSection('it'); ?>

    <div class="section-text">

        <div class="container-content-medium content-line">
            <div>
                <p class="content-list-item common-color" style="font-size: 17px">
                    Regardless you are a one person company or multinational corporation, Seldat IT will provide the same level of service as one should deserve, exceptional, dedicated, and outstanding service. All that can be changed whether you are a big, mid-sized, or small with Seldat IT.
                </p>
                <p class="content-list-item">
                    Seldat's approach is to manage IT services focusing on delivering the premier levels of convenience and security anytime, anywhere to users and customers of any size.
                </p>
                <p class="content-list-item">
                    Seldat IT employs over 150 IT professionals, developers and engineers who are ready to be dedicated to helping your company accomplish more with our experiences and professionalism.
                </p>
                <p class="content-list-item">
                    To meet your company's goals, Seldat IT will dedicate full time professional Project Managers to plan, guide, and deliver service or product aligned with your goals.
                </p>
                <p class="content-list-item">
                    SaaS for 3PL Logistics, Warehouse Management System, Transportation Management System, & EDI Service and more are available to your company's need to accelerate your business to next step and beyond.
                </p>
            </div>
        </div>
    </div>

    <section class="parallax-section"
             style="background-image: url('images/banner/services/banner_it_2.jpg');">
    </section>

    <div class="section-text flex-center">
        <div class="container-it-1000 content-line">
            <div class="it-teams-section">
                <div class="back-ground-image-wrap border">
                    <img src="images/banner/services/banner_it_7.png">
                    <div class="text-item common-color text-center">
                        <h2 style="font-weight: 900;line-height: 30px;">
                            SELDAT IT TEAMS
                        </h2>
                    </div>
                </div>
                <div class="commerce-col-2 flex-center">
                    <ul class="ul2">
                        <li>Strategic Team</li>
                        <li>Planning teams</li>
                        <li>Dedicated Design implementation team</li>
                    </ul>
                </div>
                <div class="commerce-col-2 flex-center">
                    <ul class="ul2">
                        <li>Dedicated support teams</li>
                        <li>Regulatory compliance Team</li>
                        <li>Quality Control Team</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>


    <section class="parallax-section"
             style="background-image: url('images/banner/services/banner_it_4.jpg');">

    </section>

    <div class="section-text">
        <div class="landing-title">
            <span class="landing-line "></span><span>BPO SERVICE</span><span
                class="landing-line"></span>
        </div>
        <div class="container-content-min content-line">
            <p class="content-list-item">
                With our BPO service your company will have office solutions which can be completely outsourced accounting, data entry, customer services, and other backend office services to take away burden and stress, giving you serenity to focus on what you do best and emerge your business.
            </p>
            <p class="common-title" style="font-size: 17px">
                Seldat BPO team will work with you and show you how we can make your business more efficient with our BPO service.
            </p>
        </div>
        <div class="row flex-stretch-responsive">
            <div class="col-md-6">
                <div class="panel panel-primary" style="height: calc(100% - 30px)">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>
                                <strong>DATA ENTRY AND DOCUMENT PROCESSING</strong>
                            </h4>

                        </div>
                    </div>
                    <div class="panel-body">
                        <ul class="list-arrow-right list-padding-10 ul">
                            <li>Historical Documents Data Entry</li>
                            <li>Forms and Applications Processing</li>
                            <li>Invoice Processing</li>
                            <li>Mail Responses Processing</li>
                            <li>Mailing Address Standardization and Hygiene Processing</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary" style="height: calc(100% - 30px)">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>
                                <strong>SCANNING AND ARCHIVING</strong>
                            </h4>

                        </div>
                    </div>
                    <div class="panel-body">
                        <ul class="list-arrow-right list-padding-10 ul">
                            <li>Accounting and Finance Documents</li>
                            <li>Bank and Insurance Application Documents</li>
                            <li>Back-office documents (contracts, import & export, policy)</li>
                            <li>Catalogs, Yearbooks, Law Books</li>
                            <li>Engineering Drawings & Construction Documents</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row flex-stretch-responsive">
            <div class="col-md-6">
                <div class="panel panel-primary" style="height: calc(100% - 30px)">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>
                                <strong>CALL CENTER</strong>
                            </h4>

                        </div>
                    </div>
                    <div class="panel-body">
                        <ul class="list-arrow-right list-padding-10 ul">
                            <li>Technical Support</li>
                            <li>Live Web Chat Support</li>
                            <li>Direct Voice Support</li>
                            <li>Order Entry Processing</li>
                            <li>Customer Service Support</li>
                            <li>Lead Generation</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary" style="height: calc(100% - 30px)">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>
                                <strong>IMAGE PROCESSING SERVICES</strong>
                            </h4>

                        </div>
                    </div>
                    <div class="panel-body">
                        <ul class="list-arrow-right list-padding-10 ul">
                            <li>Background releasing, replacing, clipping path</li>
                            <li>Color correction (e.g. adjusting contrast, brightness, blend)</li>
                            <li>Skin tone, classes, eyes and teeth correction</li>
                            <li>Mod& retouching, spot & dust removal, mannequin removal</li>
                            <li>Objects composing</li>
                            <li>Reflection/shadow removal</li>
                            <li>Photo restoration</li>
                            <li>Photo conversion</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul>
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>
<?php get_template_part('before-footer') ?>
<?php get_footer(); ?>
