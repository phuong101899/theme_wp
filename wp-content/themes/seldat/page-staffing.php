<?php
/*
Template Name: Template Staffing
*/
get_header();

?>


<div id="dynamic-content">

    <div class="banner-static common-banner"
         style="background: url('<?php echo get_banner_page('staffing') ?:"images/banner/services/staffing_banner.jpg" ?> ') no-repeat center center">
        <div class="banner-text"><?php the_title(); ?></div>
    </div>

    <div class="section-text">
        <div class="container-content-1000">
            <div class="row flex-center no-padding">
                <div class="col-md-6 content-line text-right">
                    <span class="common-title">Seldat Staffing</span>
                    is a premiere provider of cost effective staffing solutions with more than 5 years of expertise. A professional organization established programs and systems developed to enhance productivity and improve profitability.
                </div>
                <div class="col-md-3">
                    <img src="images/banner/services/staffing_1.png"
                         class="img-responsive">
                </div>
                <div class="col-md-3">
                    <img src="images/banner/services/staffing_2.png"
                         class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('video'); getVideoSection('staffing'); ?>

    <div class="section-text">
        <div class="row flex-center" style="align-items: flex-start">
            <div class="col-md-5" style="padding-top: 5px">
                <div class="staffing-left-title">
                    <?php echo sprintf('FOCUS ON LIGHT %s INDUSTRIAL & OFFICE %s ADMINISTRATIVE STAFFING','<br/>','<br/>');  ?>:
                </div>
                <div style="text-align: right; margin-top: 20px">
                    <?php echo sprintf('Temporary, temp to perm and direct hire %s Competitive rates and volume discounts','<br/>') ?>
                </div>
            </div>
            <div class="col-md-7">
                <ul class="list-arrow-right list-padding-10">
                    <li>
                        Customized approach to all staffing needs, including Onsite Management
                    </li>
                    <li>
                        Professional and experienced staff support, including Bilingual client support and service
                    </li>
                    <li>
                        Full service hiring process from screening to training and handling payroll, taxes, insurance and worker's comp.
                    </li>
                    <li>Background check and drug screening available upon request</li>
                    <li>DOT Transportation for employees to and from their work assignments</li>
                    <li>Forklift safety training and certification</li>
                    <li>Full-time certified Worker's Comp/Safety Management administrator</li>
                    <li>Fully ACA Compliant</li>
                </ul>
            </div>
        </div>
    </div>
    <section class="parallax-section"
             style="background-image: url('images/banner/services/staffing_section_3.jpg');">
    </section>

    <div class="section-text">
        <div class="flex-center-recruiting">
            <div class="wrap-image-text" style="flex-wrap: wrap">
                <div class="text-left">
                    <div class="staffing-recruiting-column">
                        <h3 class="common-title">RECRUITMENT & PLACEMENT EXPERTISE</h3>
                        <ul class="list-angle-double-right" style="line-height: 30px">
                            <li><span> LIGHT INDUSTRIAL STAFFING & RECRUITMENT INCLUDE </span>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="list-angle-right">
                                            <li>Assemblers</li>
                                            <li>Forklift/ Machine Operators</li>
                                            <li>General Labor</li>
                                            <li>Liine Leads</li>
                                            <li>Pickers/Packers</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-angle-right">
                                            <li>Quality Control</li>
                                            <li>Shipping & Receiving Clerks</li>
                                            <li>Supervisors</li>
                                            <li>Warehouse Management</li>
                                        </ul>
                                    </div>
                                </div></li>
                            <li><span> OFFICE/ADMINISTRATIVE STAFFING RECRUITMENT INCLUDE </span>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="list-angle-right">
                                            <li>Administrative Assistants</li>
                                            <li>Call Center Representatives</li>
                                            <li>Customer Service</li>
                                            <li>Data Entry</li>
                                            <li>Executive Assistants/ File Clerks</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-angle-right">
                                            <li>General Office Assistants</li>
                                            <li>Office Managers</li>
                                            <li>Receptionists/ Front Desk Personnel</li>
                                            <li>Telemarketing</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="image-column">
                    <div class="back-ground-image-wrap">
                        <img src="images/banner/services/staffing_3.png" class="img-responsive pull-left">
                        <div class="text-item text-center">
                            <div style="color: #fff">
                                <h3 style="font-weight: bold">Infrastructure & Client Support</h3>
                                <ul class="bg-inside-list content-line" style="list-style: none">
                                    <li>3 Jersey Offices</li>
                                    <li>Dedicated Management & Recruiters available 24/7</li>
                                    <li>On Site Management</li>
                                </ul>

                                <ul class="list-inline">
                                    <li><span class="glyphicon glyphicon-map-marker"></span> EDISON</li>
                                    <li><span class="glyphicon glyphicon-map-marker"></span> DAYTON</li>
                                    <li><span class="glyphicon glyphicon-map-marker"></span>
                                        BURLINGTON</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>


        </div>
    </div>
    <section class="parallax-section"
             style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul>
                    <li><span class="parallax-text">SUPPORT@SELDATINC.COM</span></li>
                    <li><span class="parallax-text text-boxed">+1-732-348-0000</span></li>
                    <li><span class="parallax-text ">1900 RIVER ROAD,</span></li>
                    <li><span class="parallax-text">BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>

<?php get_template_part('before-footer') ?>
<?php get_footer(); ?>
