<?php
/*
Template Name: Template Supply Chain Solutions
*/
get_header();

?>


<div id="dynamic-content">

    <div class="banner-static common-banner" style="background: url('<?php echo get_banner_page('supply')?:"images/banner/services/banner_supply_2000.jpg" ?>') no-repeat center center">
        <div class="banner-text">
            <?php the_title(); ?>
        </div>
    </div>

    <div class="section-text">
        <div class="container-content-medium flex-center">
            <img src="images/banner/services/banner_supply_chain_1.jpg" style="margin-bottom: 50px" />
            <p class="text-center" style="font-size: 16px">
                <span class="common-title" style="font-size: 22px">Supply chain solutions</span>, we combine a process-driven approach with action to deliver measurable business value. So you can be more efficient. Leaner. More agile. So you can free up cash flow. Save money. Deliver smarter.
            </p>
        </div>
    </div>

    <?php get_template_part('video'); getVideoSection('supply'); ?>

    <div class="section-text">
        <div class="container-content-medium">
            <div class="landing-title" style="padding: 0">
                <span class="landing-line "></span><span>PERFORMED SERVICES</span><span
                    class="landing-line"></span>
            </div>
            <h4 class="text-center" style="color: #000;margin-bottom: 80px">Performed services in supply chain solutions</h4>
            <div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-outline" id="hash-3pl">
                        <div  class="panel-heading clearfix supply-heading" role="button" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#3pl">
                            <div class="panel-title pull-left">
                                FULL 3PL SERVICES
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#3pl" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="3pl" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-5">
                                            <img src="images/banner/services/supply_1.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-7">
                                            <p>
                                                Seldat is a full 3PL Services  with five distribution facilities in
                                                New Jersey, Los Angeles and Canada totally over 1 million square feet of
                                                space. Seldat's supply chain solutions provide the cost flexibility and
                                                efficiency that allow our clients to outsource all, or part of their supply
                                                chain needs, by providing an end-to-end logistics solution.
                                            </p>

                                            <p class="common-color" style="font-size: 1.2em">
                                                Seldat's commitment to technology, and highly experienced management team
                                                provide a streamlined process that makes Seldat an industry leader.
                                            </p>

                                        </div>
                                    </div>
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">Seldat provides unmatched supply chain management functions that include:</h4>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12 supply-padding-left">
                                            <h4 class="list-triangle-bullet">WAREHOUSING</h4>
                                            <p>
                                                With facilities on the East and West Coasts of the US and Canada, Seldat's facilities are strategically located within close proximity to the busiest ports in each region, allowing Seldat to provide efficient warehouse and rack storage solutions for our clients.
                                            </p>

                                            <p>
                                                Our proprietary Warehouse Management Software utilizes a unique bar code scanning system that provides our clients with enhanced tracking and instant access to all inventory and order information.
                                            </p>


                                            <h4 class="list-triangle-bullet">DISTRIBUTION & TRANSPORTATION MANAGEMENT</h4>
                                            <p>
                                                Seldat's strategically located facilities allow us to pick up clients' containers at the port, receive and unload them at our facilities and ship their orders in a quick and timely fashion. This is accomplished by using our own fleet of trucks & equipment, which allow us to process inbound and outbound freight without delay. Ask about Seldat Transportation Services for more details on the variety of available solutions.
                                            </p>
                                            <h4 class="list-triangle-bullet">E-COMMERCE SOLUTIONS/PICK & PACK/DROPSHIPPING</h4>
                                            <p>
                                                Seldat clients can utilize our state of the art assembly lines to achieve a cost effective solution to their E-Commerce, Pick & Pack, and Drop Ship order fulfillment requirements. Seldat will customize a solution to fit our client's needs. Order fulfillment within 24 hours of receipt, makes our clients a higher value partner to their retail partners than their competitors.
                                            </p>
                                            <h4 class="list-triangle-bullet">VALUE ADDED SERVICES </h4>
                                            <p>
                                                Seldat offers quality packaging solutions that include a variety of cost cutting services that exceed client expectations. These services include but are not limited to: Ticketing, Labeling, Sorting, Repacking, QC Inspection, Product Testing and many others. Please provide us with the details of your specific needs and we will provide you with a customized quote.
                                            </p>
                                            <h4 class="list-triangle-bullet">RETAIL SERVICES </h4>
                                            <p>
                                                Seldat's retail services arm is uniquely positioned to provide Retailers an outsourced solution to their supply chain needs in a cost effective manner. These services include: product ticketing/labeling, allocation labels, carton labeling, bills of lading, freight direct to store, creating pallets, repacking/spinning, mixing product, applying anti-theft alarms and much more.
                                            </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-outline" id="hash-ecommerce">
                        <div class="panel-heading clearfix supply-heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#ecommerce">
                            <div class="panel-title pull-left">
                                E-COMMERCE & DROPSHIPPING SERVICES
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ecommerce" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="ecommerce" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">
                                                Allow our team of professionals to
                                                handle all your e-commerce
                                                fulfillment needs, we will provide the
                                                following services:
                                            </h4>
                                            <ul class="list-arrow-right list-padding-10">
                                                <li>Our specially designed e-commerce area will designate locations specifically designed for your product and account.</li>
                                                <li>We strive to have your order picked, packed and shipped on the same day.</li>
                                                <li>Your inventory levels will be automatically restocked every day for the next shipment day, so optimal inventory levels are always maintained.</li>
                                                <li>All inventies and orders are scanned and double checked for 100% accuracy provide tracking of your shipments.</li>
                                                <li>Our automated packaging process reduces costs by limiting wasted packaging materials and improving efficiencies.</li>
                                            </ul>
                                            <h4 class="common-color" style="font-weight: bold;padding: 15px 0">We meet your E-Commerce fulfillment needs for:</h4>
                                        </div>
                                    </div>

                                    <div class="row no-padding flex-stretch">
                                        <div class="col-md-6">
                                            <div class="commerce-col-2 flex-center">
                                                <ul>
                                                    <li>Amazon.com - FBA & MF</li>
                                                    <li>Third Party Online Retail</li>
                                                    <li>Flash Sales</li>
                                                    <li>Supplier Direct Website</li>
                                                </ul>
                                            </div>

                                        </div>
                                        <div class="col-md-6 commerce-col-3 flex-center" style="margin-right: 15px">
                                            SELDAT WILL DESIGN A CUSTOMIZED PLAN TO MEET YOUR NEEDS
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-outline" id="hash-retail">
                        <div class="panel-heading clearfix supply-heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#retail">
                            <div class="panel-title pull-left">
                                RETAIL DISTRIBUTION SERVICES
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#retail" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="retail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-5">
                                            <img src="images/banner/services/supply_2.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-7">
                                            <p>
                                                Seldat is at the forefront of distribution management for retail/chain stores. As a leader in the Logistics and Fulfillment industry, Seldat provides solutions for retail businesses worldwide, with the goal of offering complete, efficient and strategic solutions from the source to the shelf.
                                            </p>

                                            <p class="common-color" style="font-size: 1.2em">
                                                Our flexibility allows us to quickly adapt to change, putting Seldat in position to optimize and execute the right inventory, at the right location, at the right time; resulting in real-time responsiveness.
                                            </p>

                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12 supply-padding-left">
                                            <h4 class="list-triangle-bullet">RECEIVING </h4>
                                            <p>
                                                Our professional routing department will schedule your entire incoming receiving, handle all specialized requests, and keep you informed of the scheduled inbound shipments. Upon receipt of your merchandise, we guarantee the verification of the physical delivery to your purchase orders by our team of receiving specialists.
                                            </p>

                                            <p>
                                                The inventory is entered into our WMS system for your real time access. Our customer ser-vice team will contact you with any discrepancies and are available to answer all your ques-tions.
                                            </p>


                                            <h4 class="list-triangle-bullet">PROCESSING </h4>
                                            <p>
                                                We can handle all your processing needs such as ticketing, re-packing, and mixing based on your request.We provide a high level of quality control to assure that all these processes are carried out with 100% accuracy.
                                            </p>

                                            <p>
                                                All cartons are double checked to ensure that the information is processed efficiently. We have the ability to label all cartons with the style number, description, case pack, price per unit, and department. This process ensures inventory control and provides for fast and accurate receiving at the store level.
                                            </p>

                                            <h4 class="list-triangle-bullet">TRANSPORTATION  </h4>
                                            <p>
                                                With our own fleet of trucks and drivers, we can provide the right transportation solution to meet all of your delivery requirements. We can leverage our own carrier network, or work with your approved carvers to support inbound logistics, direct-store-distribution, direct-to-consumer delivery, and returns.
                                            </p>

                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 class="common-color" style="font-weight: bold">Seldat understands that timing and accuracy are key factors to a successful retail business. </h4>
                                            <p>
                                                Whether you are a small chain of 5 stores or a regional or national chain, Seldat can handle all your retail chain store distribution needs.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-outline" id="hash-transportation">
                        <div class="panel-heading clearfix supply-heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#transportation">
                            <div class="panel-title pull-left">
                                TRANSPORTATION MANAGEMENT SERVICES
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#transportation" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="transportation" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-5">
                                            <img src="images/banner/services/supply_3.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-7">
                                            <p>
                                                Our main objective is to provide our clients with the highest quality service while prodding cost savings and superior service. Because we have our own fleet of trucks and equipment, we can process inbound and outbound freight without delays.
                                            </p>

                                            <p class="common-color" style="font-size: 1.2em">
                                                The Seldat team is experienced in handling and executing all your logistical and freight needs.
                                            </p>

                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">Transportation Features: </h4>
                                            <ul class="list-arrow-right list-padding-6">
                                                <li>Purchase Order verification</li>
                                                <li>Dispatched shipments according to client's orders</li>
                                                <li>Shipment tracking</li>
                                                <li>An effective fleet service to ensure on-time transportation</li>
                                                <li>Deliveries and pick-ups on a regular basis throughout the year</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">Our Freight Services Include: </h4>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12 supply-padding-left">

                                            <h4 class="list-triangle-bullet">DRAYAGE </h4>
                                            <p>
                                                Our exclusive Drayage service provides highly competitive pricing that will reduce your transportation costs and improve your inventory turns.
                                            </p>

                                            <p>
                                                By utilizing our own dedicated fleet of state of the art trucks and drivers, we make quick pick-ups from the Ports of Los Angeles/Long Beach and New Jersey/New York Ports, and deliver to your distribution facilities without delay.
                                            </p>

                                            <h4 class="list-triangle-bullet">DEDICATED TO STORE DELIVERY </h4>
                                            <p>
                                                Seldat works with major retailers across the US to provide customized solutions and ser-vices for store deliveries. Data reports are provided to our customers for sufficient audit trails and on-time performance measurements. We participate in "green fleet" deliveries to contribute to environmental benefits.
                                            </p>
                                            <h4 class="list-triangle-bullet">TIME CRITICAL SERVICE </h4>
                                            <p>
                                                The goal of expedited freight shipping is to move through the supply chain as quickly as possible and provide on-time delivery. By utilizing this process we create a one-stop approach to aid your critical shipment needs.
                                            </p>

                                            <h4 class="list-triangle-bullet">CONSOLIDATION DECONSOLIDATION SERVICES </h4>
                                            <p>
                                                We provide consolidation services throughout our business units across the country and the execution of freight into full truck loads for our Retail Partners Distribution Centers. In addition, we offer deconsolidation services that include facilities that are located near ports to expedite the process of goods transported from the port to your stores.
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-outline" id="hash-forwarding">
                        <div class="panel-heading clearfix supply-heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#forwarding">
                            <div class="panel-title pull-left">
                                CUSTOM BROKERAGE AND INBOUND FREIGHT FORWARDING
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#forwarding" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="forwarding" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-5">
                                            <img src="images/banner/services/supply_4.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-7">
                                            <h4 class="common-color" style="font-weight: bold">US CUSTOMS BROKERAGE</h4>
                                            <p>
                                                As a full service company, we realize the importance of the costs involved in US Customs duties, fitting of clearances, documentation flow, tracking of the goods, coordination with the shipping lines, ports and depots.
                                            </p>

                                            <p class="common-color" style="font-size: 1.2em">
                                                With a team having extensive field experi-ence and advised by the top law firms, Seldat is proud to offer highest level of professional services to import clients.
                                            </p>

                                        </div>
                                    </div>

                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">US Customs Brokerage Features: </h4>
                                            <ul class="list-arrow-right list-padding-6">
                                                <li>Nationwide Customs Clearance</li>
                                                <li>ACH, ABI and ACE certified system</li>
                                                <li>Duty minimizing initiatives backed by top NYC attorneys</li>
                                                <li>100% compliance record</li>
                                                <li>C-TPAT Re-Validation experts</li>
                                                <li>Easy and interactive tools to track daily container clearances and deliveries</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-12">
                                            <img src="images/banner/services/supply_5.jpg" class="img-responsive"/>
                                        </div>
                                    </div>


                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">IN-BOUND FORWARDING </h4>
                                            <p>
                                                Having secured volume rates in the market, Seldat could match or better any competitor's freight quote. With agent network worldwide, in-bound freight service picks up from all major origins.
                                            </p>
                                            <ul class="list-arrow-right list-padding-6">
                                                <li>Air and Ocean cargo from all major origins</li>
                                                <li>Highly competitive freight rates</li>
                                                <li>ISF and AM5 filing at origin for highest compliance</li>
                                                <li>Yearly contracts to accommodate capacity issues</li>
                                                <li>Inspection services at the origin</li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-outline" id="hash-canadian">
                        <div class="panel-heading clearfix supply-heading" role="button" data-toggle="collapse" data-parent="#accordion" href="#canadian">
                            <div class="panel-title pull-left">
                                CANADIAN MASTER DISTRIBUTION SERVICES
                            </div>
                            <div class="pull-right">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#canadian" class="collapsed">
                                    <i class="toggle-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div id="canadian" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body">
                                <div class="content-line">
                                    <div class="row no-padding supply-first-row">
                                        <div class="col-md-5">
                                            <img src="images/banner/services/supply_6.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-7">
                                            <p>
                                                When it comes to selling product in Canada, most Companies try to do it themselves. When they do, they pay duties that are higher than the duties paid by Canadian companies. This increases the cost of goods sold and reduces profit margins for Non Cana-dian Companies selling into Canada.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <p class="common-color" style="font-size: 1.2em">
                                                What if there was a way for Companies outside Canada to sell product into Canada, and pay the same, reduced duties, paid by Canadian companies?
                                            </p>

                                            <p>
                                                Say hello to the Canadian Master Distribution Agreement. Without this agreement, the import duty is calculated on the SELLING price of the goods being imported. With this agreement, the duty is calculated based on the FOB price from the factory.
                                            </p>

                                            <div class="flex f-center flex-wrap-small">
                                                <div class="common-title" style="padding: 10px 10px 10px">
                                                    <ul class="ul-default text-right">
                                                        <li>Example:</li>
                                                        <li>FOB Price: $10</li>
                                                        <li>Selling Price: $20</li>
                                                        <li>Duty Rate: 18%</li>
                                                    </ul>
                                                </div>
                                                <div class="flex-stretch">
                                                    <div class="common-border text-left" style="padding: 10px">
                                                        <div>
                                                            <span class="common-title">Without</span>
                                                            Master Distribution Agreement
                                                        </div>

                                                        <div>
                                                            Duty is calculated by taking 1896 of the <span class="common-title">Selling Price.</span> $20 x 189.6= 53.60 Duty
                                                        </div>

                                                    </div>
                                                    <div class="common-border text-left" style="padding: 10px">
                                                        <div>
                                                            <span class="common-title">WITH</span>
                                                            Master Distribution Agreement
                                                        </div>

                                                        <div>
                                                            Duty is calculated by taking 18% of the <span class="common-title">FOB</span>. $10 x 189.6= 51.80 <span class="common-title">Duty Savings</span> - 50%
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <p style="margin-top: 40px">
                                                Because Seldat is a Canadian Corporation with a distribution center in Mississauga, Ontario we can offer a Master Distribution Agreement to Non Canadian Companies, saving these companies significant money.
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row no-padding">
                                        <div class="col-md-12">
                                            <h4 style="font-weight: bold">
                                                Benefits of utilizing Master Distribution Agreements in Canada include:
                                            </h4>

                                            <ul class="list-arrow-right list-padding-6">
                                                <li>Duty Savings</li>
                                                <li>Access to our state of the art distribution center in Mississauga, Ontario</li>
                                                <li>Ability to stock and replenish inventory in Canada for distribution in Canada</li>
                                                <li>Assistance with customs clearance</li>
                                                <li>Get the benefits of a Canadian company without the requirement to file Canadian Tax Returns</li>
                                                <li>Our Sales team is always available to answer any questions and provide additional details.</li>
                                            </ul>

                                            <div class="alert alert-warning common-color" style="font-size: 1.2em;background-color: #f7f7f7;border-color: #e9e9e9;margin-top: 30px;margin-bottom: 0" role="alert">
                                                Our Sales team is always available to answer any questions and provide additional details.
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="parallax-section" style="background-image: url('images/parallax/parallax4.jpg');">
        <div class="parallax-modal">
            <div class="container">
                <ul  class="wow" data-animation="bounceIn" data-timeout="200">
                    <li>
                        <span class="parallax-text" >SUPPORT@SELDATINC.COM</span>
                    </li>
                    <li>
                        <span class="parallax-text text-boxed">+1-732-348-0000</span>
                    </li>
                    <li>
                        <span class="parallax-text " >1900 RIVER ROAD,</span>
                    </li>
                    <li>
                        <span class="parallax-text" >BURLINGTON NJ, UNITED STATES</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>


</div>

<?php get_template_part('before-footer') ?>


<?php get_footer(); ?>
<script>

    $(document).ready(function () {
        $('.panel-collapse').on('show.bs.collapse',function () {
            $(this).closest('.panel-outline').addClass('in');
        });

        $('.panel-collapse').on('shown.bs.collapse',function () {
            var hash = $(this).closest('.panel-outline').attr('id');
            if(hash) scrollToHash(hash);
        });

        $('.panel-collapse').on('hide.bs.collapse',function () {
            $(this).closest('.panel-outline').removeClass('in');
        });

        $(window).load(function () {
            var hash = window.location.hash;
            if(hash)
            {
                $(hash).find('.panel-collapse').collapse('show');
            }

            return;
        });





        function scrollToHash(hash) {
            var nav_menu = $('#nav-menu').css('position');
            var nav_menu_height = 0;
            if(nav_menu == 'static') nav_menu_height = $('#nav-menu').outerHeight();

            var element = $('#'+hash);
            var top = element.position().top;
            var heading = element.find('.panel-heading').outerHeight();

            $('html,body').animate({
                scrollTop: top - heading - nav_menu_height
            }, 700);

            window.history.pushState(null,'',window.location.pathname+'#'+hash);
        }
    });


</script>