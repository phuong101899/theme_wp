/**
 * 
 */

$(document).ready(function(){
	$(function() {

		var $window  = $(window);
		$window.on('scroll', revealOnScroll);

		function revealOnScroll() {
			var scrolled = $window.scrollTop(),
					win_height_padded = $window.height() * 1.1;

			// Showed...
			$(".wow:not(.animated)").each(function () {
				var $this     = $(this),
						offsetTop = $this.offset().top;

				if (scrolled + win_height_padded > offsetTop) {
					if ($this.data('timeout')) {
						window.setTimeout(function(){
							$this.addClass('animated ' + $this.data('animation'));
						}, parseInt($this.data('timeout'),10));
					} else {
						$this.addClass('animated ' + $this.data('animation'));
					}
				}
			});
			// Hidden...

			/*
			 //Get list effect name
			 var effectList = [];
			 $('.wow').each(function () {
			 var effect = $(this).data('animation');
			 if(effectList.indexOf(effect) == -1)
			 {
			 effectList.push(effect);
			 }
			 });

			 $(".wow.animated").each(function (index) {
			 var $this     = $(this),
			 offsetTop = $this.offset().top;
			 if (scrolled + win_height_padded < offsetTop)
			 {
			 $(this).removeClass('animated '+effectList.join(' '));
			 }
			 });*/
		}
		revealOnScroll();
	});
});