(function () {
    function buildWidthMenuDetail($this) {

        var $dropdown_menu_extend_width = $('.dropdown-menu-extend').outerWidth();
        var $dropdown_menu = $('.dropdown-menu-extend .dropdown-menu').outerWidth();

        var $dropdown_menu_sub_lvl2 = $('.dropdown-menu-extend .dropdown-menu-sub-lvl2').outerWidth();
        if(!!$this)
        {
            try{
                $dropdown_menu_sub_lvl2 = $this.find('.dropdown-menu-sub-lvl2').outerWidth();
            }catch (e){}
        }



        $('.dropdown-menu-extend .dropdown-menu .menu-detail').css({'width':$dropdown_menu_extend_width - $dropdown_menu - $dropdown_menu_sub_lvl2});
    }

    $(window).on('load resize',buildWidthMenuDetail);

    var dropdown_menu1 = '#dropdown_menu-service-1';
    var dropdown_menu2 = '#dropdown_menu-service-2';

    $('.mega-menu').hover(
        function() {
            $(dropdown_menu2).stop(true, true).fadeIn('fast',buildWidthMenuDetail);

        },
        function() {
            $(dropdown_menu2).stop(true, true).delay(200).fadeOut();
        }
    );

    /*$(dropdown_menu1).hover(
        function() {

            $(dropdown_menu2).delay(400).fadeIn();

            $(this).stop(true, true).delay(200).fadeOut();

            buildWidthMenuDetail();
        },
        function() {
            $(this).stop(true, true).delay(200).fadeOut();
        }
    );*/



    $(dropdown_menu2).hover(
        function() {

            $(this).stop(true, true);
            $('.mega-menu').addClass('open');
            buildWidthMenuDetail();
        },
        function() {
            $(this).stop(true, true).delay(200).fadeOut();
            $('.mega-menu').removeClass('open');
        }
    );

    $('.dropdown-menu-sub').hover(
        function () {
            var $this = $(this);
            $('.dropdown-menu-sub').each(function () {
                $(this).removeClass('open');
            });
            buildWidthMenuDetail($this);
            $this.addClass('open');

            if($this.find('.dropdown-menu-sub-lvl2').length)
            {
                var position = $this.position();
                $this.find('.dropdown-menu-sub-lvl2').css({'top' : -position.top+'px'});
            }
        }
    );

    $('.dropdown-menu-sub-lvl2 >li').hover(
        function () {
            var $this = $(this);
            $('.dropdown-menu-sub-lvl2 >li').each(function () {
                $(this).removeClass('open');
            });
            $this.addClass('open');
        }
    );


    $('.dropdown-menu-sub-lvl3 >li').hover(
        function () {
            var $this = $(this);
            $('.dropdown-menu-sub-lvl3 >li').each(function () {
                $(this).removeClass('open');
            });
            $this.addClass('open');
        }
    );


    function getHeightExtendMenu() {
        var nav_menu = $('#nav-menu');
        var height = $(window).height() - (nav_menu.offset().top + nav_menu.outerHeight());

        $('.dropdown-menu-extend .dropdown-menu-sub-lvl1').css({'height' : height});
    }
    getHeightExtendMenu(); // on window load

})();