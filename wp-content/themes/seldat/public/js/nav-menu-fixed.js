$(document).ready(function (event) {
    var main_menu = $('#nav-menu');
    var main_menu_top = $('#nav-menu').position().top;


    $(window).scroll(function(event){

        if($(this).scrollTop() > main_menu_top)
        {
            main_menu.css({
                'position' : 'fixed',
                'top'       : 0,
                'z-index'   : 999,
                'width'     : '100%',
                'background'    : '#fff',
                'box-shadow' : '0 1px 1px rgba(100,100,100,0.1)'
            });
        }
        else
        {
            main_menu.css({
                'position' : 'relative',
                'top'       : 'auto',
                'z-index'   : 999,
                'width'     : '100%',
                'box-shadow' : 'none'
            });
        }
    });
});