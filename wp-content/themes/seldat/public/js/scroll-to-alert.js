$(document).ready(function(e){
	
	function scrollToHash(hash) {
        var nav_menu = $('#nav-menu').css('position');
        var nav_menu_height = 0;
        if(nav_menu == 'static') nav_menu_height = $('#nav-menu').outerHeight();

        var element = $(hash);
        
        var top = element.position().top;
        var height = element.outerHeight();


        $('html,body').animate({
            scrollTop: top - nav_menu_height - height
        }, 700);
    }
	
	$(window).load(function () {
        var hash = window.location.hash;
        if(hash)
        {
        	scrollToHash(hash);
        }

        return;
    });
	
	$('form').submit(function(){
		$(this).find('button:submit')
			.html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i><span class="sr-only">Loading...</span>')
			.prop('disabled',true);
	});
});