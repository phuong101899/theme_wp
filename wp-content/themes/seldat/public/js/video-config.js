(function () {
    $(document).ready(function () {
        var options = {
            containment : '#show-video-bg',
            autoPlay : false,
            opacity : 1,
            showControls : false,
            mute : false,
            stopMovieOnBlur : false,
            loop : 0,
            //backgroundUrl :'images/logo/video-bg.png'
        };

        var playerID = '#show-video-bg';

        function buildVideo(options) {
            jQuery(playerID).YTPlayer(options);
        }

        buildVideo(options); // init


        var playButton = '.play-btn';
        jQuery(document).on('click',playButton,function (event) {
            event.preventDefault();
            event.stopPropagation();
            jQuery(playerID).YTPPlay();
            $(this).hide();
        });
        
        jQuery(document).on('click','.video-container',function(event){
        	jQuery(playerID).YTPPause();
        });


        jQuery(playerID).on('YTPEnd YTPPause',function () {
            jQuery(playButton).show();
        });
        
        jQuery(playerID).on('YTPPlay',function () {
            jQuery(playButton).hide();
        });
        
        

        
        var first_play = false;
        
        $(document).on('scroll',function(){
            if($('.video-bg').length)
            {
                var flag = isScrolledIntoView('.video-bg');

                if(flag==1 && !first_play)
                {
                    jQuery(playerID).YTPPlay();
                    first_play = true;
                }
                else if(flag == 2)
                {
                    try{
                        jQuery(playerID).YTPPause();
                    }catch (e){}

                }
            }


        });
        
        
        function isScrolledIntoView(elem)
        {
        	var nav_height = $('#nav-menu').height();
        	
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top - nav_height;
            var elemBottom = elemTop + $(elem).height();
                        
            if(docViewTop < elemTop)
            {
            	return 0;
            }
            else if(docViewTop >= elemTop && docViewTop <= elemBottom)
            {
            	return 1;
            }
            else if(docViewTop > elemBottom)
            {
            	return 2;
            }
            
          
        }
    });


})();