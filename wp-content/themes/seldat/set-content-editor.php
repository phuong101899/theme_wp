<?php

add_filter( 'default_content', 'set_editor_content', 10, 2 );

function set_editor_content( $content, $post ) {

    switch( $post->post_type ) {
        case 'careers':
            $content = file_get_contents(get_template_directory_uri().'/content/career-content.php');
            break;
    }

    return $content;
}