<?php global $seldat_options; ?>

<?php if(isset($seldat_options['home-slides-id']) && count($seldat_options['home-slides-id'])>0): ?>
<div id="slide-images">
    <div class="rev_slider_wrapper fullwidthbanner-container">
        <!-- START REVOLUTION SLIDER 5.2.5.4 fullwidth mode -->
        <div id="home-slider" class="rev_slider fullwidthabanner"
             style="display: none;" data-version="5.2.5.4">



            <ul>
                <!-- SLIDE  -->
                <?php foreach ($seldat_options['home-slides-id'] as $key=>$item): ?>
                <li data-index="rs-<?php echo $key ?>" data-transition="fade"
                    data-slotamount="default" data-hideafterloop="0"
                    data-hideslideonmobile="off" data-easein="default"
                    data-easeout="default" data-masterspeed="300"
                    data-thumb="<?php echo $item['image'] ?>" data-rotate="0"
                    data-saveperformance="off" data-title="Slide">

                    <!-- MAIN IMAGE -->

                    <img src="<?php echo $item['image'] ?>" alt="<?php echo $item['title'] ?>"
                         title="<?php echo $item['title'] ?>" width="2050" height="600"
                         data-bgposition="center center" data-kenburns="on"
                         data-duration="10000" data-ease="Linear.easeNone"
                         data-scalestart="100" data-scaleend="115" data-rotatestart="0"
                         data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"
                         class="rev-slidebg" data-no-retina>

                </li>
                <?php endforeach; ?>
            </ul>

            <div class="tp-bannertimer tp-bottom"
                 style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>
<?php endif; ?>