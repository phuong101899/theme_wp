<?php
if ( ! class_exists( 'Redux' ) ) {
    return;
}

$opt_name = "seldat_options";

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    'display_name'         => $theme->get( 'Name' ),
    'display_version'      => $theme->get( 'Version' ),
    'menu_title'           => __( 'SELDAT Options', 'seldat' ),
    'customizer'           => true,
);

Redux::setArgs( $opt_name, $args );

Redux::setSection( $opt_name, array(
    'title'  => __( 'General', 'seldat' ),
    'id'     => 'basic',
    'desc'   => __( 'Basic field with no subsections.', 'seldat' ),
    'icon'   => 'el el-home',
    'fields' => [
        [
            'id'    => 'logo-switch',
            'type'  => 'switch',
            'title' => __('Enable Image Logo','seldat'),
            'compiler'  => 'bool',
            'desc'      => __('Do you want to enable image logo?','seldat'),
            'on'     => __('Enable','seldat'),
            'off'    => __('Disabled','seldat')
        ],
        [
            'id'    => 'logo-upload',
            'type'  => 'media',
            'title' => __('Logo Image','seldat'),
            'desc'  => __('Image that you want to use as logo.','seldat')
        ],
        [
            'id'    => 'mail-system',
            'type'  => 'text',
            'title' => __('Mail System','seldat'),
            'desc'  => __('Config a no-reply mail sender','seldat')
        ],
        [
            'id'    => 'mail-system-password',
            'type'  => 'password',
            'title' => __('Mail System Password','seldat'),
        ],
        [
            'id'    => 'mail-contact',
            'type'  => 'text',
            'title' => __('Mail Contact','seldat'),
            'desc'  => __('Config a mail receive','seldat')
        ],
        [
            'id'    => 'mail-career',
            'type'  => 'text',
            'title' => __('Mail Career','seldat'),
            'desc'  => __('Config a mail career','seldat')
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Companies Gallery', 'seldat' ),
    'id'     => 'gallery',
    //'desc'   => __( 'Basic field with no subsections.', 'seldat' ),
    'icon'   => 'el el-th-list',
    'fields' => [
        [
            'id'    => 'gallery-id',
            'type'  => 'gallery',
            'title' => __('Add/Edit Gallery','seldat'),
            'desc'  => __('Companies gallery in about page','seldat')
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Home Page', 'seldat' ),
    'id'     => 'home-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'home-slides-id',
            'type'  => 'slides',
            'title' => __('Slides Options','seldat'),
        ],
        [
            'id'    => 'home-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'home-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ]
    ]
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'About Page', 'seldat' ),
    'id'     => 'about-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'about-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'about-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'about-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Supply Page', 'seldat' ),
    'id'     => 'supply-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'supply-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'supply-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'supply-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'B2B Page', 'seldat' ),
    'id'     => 'b2b-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'b2b-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'b2b-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'b2b-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );



Redux::setSection( $opt_name, array(
    'title'  => __( 'Fashion Page', 'seldat' ),
    'id'     => 'fashion-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'fashion-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'fashion-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'fashion-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'IT Services Page', 'seldat' ),
    'id'     => 'it-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'it-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'it-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'it-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Staffing Page', 'seldat' ),
    'id'     => 'staffing-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'staffing-video-url',
            'type'  => 'text',
            'title' => __('Video URL','seldat'),
        ],
        [
            'id'    => 'staffing-video-image',
            'type'  => 'media',
            'title' => __('Video background image','seldat'),
        ],
        [
            'id'    => 'staffing-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Careers Page', 'seldat' ),
    'id'     => 'careers-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'careers-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Contact Page', 'seldat' ),
    'id'     => 'contact-page',
    'icon'   => 'el el-cog',
    'fields' => [
        [
            'id'    => 'contact-banner',
            'type'  => 'media',
            'title' => __('Image banner','seldat'),
        ]
    ]
) );