
<?php

function getVideoSection($prefix)
{

    global $seldat_options;
    $video_url = $seldat_options[$prefix.'-video-url'];
    $video_image = $seldat_options[$prefix.'-video-image']['url'];
?>

<div class="video-bg">
    <div class="embed-responsive" style="height: 500px"
         id="show-video-bg"
         data-property="{videoURL : '<?php echo $video_url ?>',backgroundUrl : '<?php echo $video_image ?>'}"></div>
    <div class="video-container">
        <button class="play-btn">
            <img src="<?php echo get_template_directory_uri(); ?>/public/images/icons/play_video.png">
        </button>
    </div>
</div>

<?php } ?>