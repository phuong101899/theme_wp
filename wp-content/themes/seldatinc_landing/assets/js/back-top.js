(function () {
    var backTopID = '.back-top';
    if ($(backTopID).length) {
        var scrollTrigger = 300, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $(backTopID).show();
                } else {
                    $(backTopID).hide();
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $(backTopID).on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    $(window).scroll(function() {
        var footer = $('footer').outerHeight();
        if($(window).scrollTop() + $(window).height() + footer >= $(document).height()) {
            $(backTopID).css({'position':'absolute'});
        }
        else
        {
            $(backTopID).css({'position':'fixed'});
        }
    });
})();