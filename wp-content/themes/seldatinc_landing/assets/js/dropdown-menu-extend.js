(function () {
    function buildWidthMenuDetail() {
        var $dropdown_menu_extend_width = $('.dropdown-menu-extend').outerWidth();
        var $dropdown_menu = $('.dropdown-menu-extend .dropdown-menu').outerWidth();
        $('.dropdown-menu-extend .dropdown-menu .menu-detail').css({'width':$dropdown_menu_extend_width - $dropdown_menu});
    }

    $(window).on('load resize',buildWidthMenuDetail);

    var dropdown_menu1 = '#dropdown_menu-service-1';
    var dropdown_menu2 = '#dropdown_menu-service-2';

    $('.mega-menu').hover(
        function() {
            $(dropdown_menu2).stop(true, true).fadeIn('fast',buildWidthMenuDetail);

        },
        function() {
            $(dropdown_menu2).stop(true, true).delay(200).fadeOut();
        }
    );

    $(dropdown_menu1).hover(
        function() {

            $(dropdown_menu2).delay(400).fadeIn();

            $(this).stop(true, true).delay(200).fadeOut();

            buildWidthMenuDetail();
        },
        function() {
            $(this).stop(true, true).delay(200).fadeOut();
        }
    );



    $(dropdown_menu2).hover(
        function() {

            $(this).stop(true, true);
            $('.mega-menu').addClass('open');
            buildWidthMenuDetail();
        },
        function() {
            $(this).stop(true, true).delay(200).fadeOut();
            $('.mega-menu').removeClass('open');
        }
    );

    $('.dropdown-menu-sub').hover(
        function () {
            var $this = $(this);
            $('.dropdown-menu-sub').each(function () {
                $(this).removeClass('open');
            });
            $this.addClass('open');
        }
    );





})();