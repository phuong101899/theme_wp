(function () {
    $(document).ready(function () {
        var options = {
            containment : '#show-video-bg',
            videoURL : 'https://www.youtube.com/watch?v=yLhrOpOkMfM',
            autoPlay : false,
            startAt : 33,
            stopAt : 63,
            opacity : 1,
            showControls : false,
            mute : true,
            stopMovieOnBlur : false
        };

        var playerID = '#about-us #show-video-bg';

        function buildVideo(options) {
            jQuery(playerID).YTPlayer(options);
        }

        buildVideo(options); // init



        var playButton = '#about-us .play-btn';
        jQuery(document).on('click',playButton,function (event) {
            event.preventDefault();
            event.stopPropagation();

            var options2 = {
                //containment : '#show-video-bg',
                videoURL : 'https://www.youtube.com/watch?v=yLhrOpOkMfM',
                autoPlay : true,
                opacity : 1,
                showControls : false,
                stopMovieOnBlur : false,
                mute : false,
                loop : 0,
            };

            jQuery(playerID).YTPChangeMovie(options2);


            $(this).hide();
            console.log('rebuild video');

        });

        jQuery(document).on('click','.video-container',function () {
            var player = jQuery(playerID).YTPGetPlayer();
            var state = player.getPlayerState();

            if(state == 1)
            {
                jQuery(playerID).YTPPause();
            }
            else if(state == 2)
            {
                jQuery(playerID).YTPPlay();
            }

        });

        jQuery(playerID).on('YTPEnd',function () {
            console.log('end video');
            jQuery(playButton).show();
        });

        $('#navbar-collapse-top').on('activate.bs.scrollspy', function () {
            var currentItem = $(this).find('li.active > a');
            var hash = currentItem.attr('href');

            if(hash == '#about-us')
            {
                jQuery(playerID).YTPPlay();
            }
        });
    });


})();