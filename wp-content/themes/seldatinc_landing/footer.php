<footer>
    <nav class="navbar navbar-landing">
        <div class="container-fluid">

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav nav-effect">
                    <li class="active"><a href="#">HOME</a></li>
                    <li><a href="#">ABOUT SETDAT GROUP</a></li>
                    <li><a href="#">SERVICES</a></li>
                    <li><a href="#">CAREERS</a></li>
                    <li><a href="#">CONTACT US</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">
                            Copyright &copy; Seldatinc 2016
                        </a></li>
                    <li><a>|</a></li>
                    <li>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                    </li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</footer>