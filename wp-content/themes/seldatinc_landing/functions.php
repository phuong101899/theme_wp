<?php
define('THEME_URL',get_stylesheet_directory());
define('CORE',THEME_URL.'/core');
define('TEXT_DOMAIN','seldatinc');

define('TEMPLATE_URL',THEME_URL.'/templates');

define('ASSETS',get_template_directory_uri().'/assets/');

require_once (CORE.'/init.php');


/*
 * Theme setup
 * */

if(!function_exists('theme_setup'))
{
    function theme_setup()
    {
        $language_folder = THEME_URL.'/languages';
        load_theme_textdomain(TEXT_DOMAIN,$language_folder);
        add_theme_support('automatic-feed-links');

        add_theme_support('post-thumbnails');

        add_theme_support('post-formats',array(
            'image',
            'video',
            'gallery',
            'link'
        ));

        add_theme_support('title-tag');

        $default_background_color = array(
            'default-color' => '#e8e8e8'
        );
        add_theme_support('custom-background',$default_background_color);

        register_nav_menu('primary-menu',__('Primary Menu',TEXT_DOMAIN));

        $sidebar = array(
            'name'  => __('Sidebar name',TEXT_DOMAIN),
            'id'    => 'main-sidebar',
            'description'   => __('Default sidebar',TEXT_DOMAIN),
            'class'     => 'main-sidebar',
            'before_title'  => '<h3> class="widgettitle">',
            'after_title'   => '</h3> '

        );

        register_sidebar($sidebar);
    }
    add_action('init','theme_setup');
}


function slider_show()
{

    require_once( get_template_directory() . '/inc/class-fire-utils.php' );
    require_once( get_template_directory() . '/inc/parts/class-content-slider.php' );

    $slider = new TC_slider();
    
    return $slider->tc_slider_display();
}

//add_action('slider_show','slider_show');


