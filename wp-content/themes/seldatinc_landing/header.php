<div id="top-header">
    <nav class="navbar">
        <div class="container-fluid">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false"><img src="images/flags/24/usa.png">
                            US <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><img src="images/flags/24/usa.png"> US</a></li>
                            <li><a href="#"><img src="images/flags/24/Spain.png"> Spanish</a></li>
                            <li><a href="#"><img src="images/flags/24/Viet%20Nam.png"> Vietnam</a></li>

                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><i class="fa fa-mobile" aria-hidden="true"></i> + 1-732-348-0000</a></li>
                    <li><a>|</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-envelope"></i> info@seldatinc.com</a>
                    <li><a>|</a></li>
                    <li>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                    </li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<?php do_action('slider_show') ?>