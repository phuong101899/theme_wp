<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo ASSETS ?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php wp_head() ?>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">


    <link href="lib/wow/wow.css" rel="stylesheet">

    <link href="lib/revslider/css/revolution.css" rel="stylesheet">

    <link href="lib/YTPlayer/css/jquery.mb.YTPlayer.min.css">



    <link href="css/p.custom.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300normal,300italic,400normal,400italic,600normal,600italic,700normal,700italic,800normal,800italic|Roboto:400normal|Lato:400normal|Oswald:400normal|Open+Sans+Condensed:300normal|Raleway:400normal|Montserrat:400normal|Source+Sans+Pro:400normal|Indie+Flower:400normal|PT+Sans:400normal|Merriweather:400normal&subset=all' rel='stylesheet' type='text/css'>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#navbar-collapse-top">

<div class="wrap">
    <!--top-header-->
    <?php get_header() ?>
    <!--/top-header-->

    <div id="content-wrap">
        <!--nav-menu-->
        <?php get_template_part('parts/nav','menu'); ?>
        <!--/nav-menu-->

        <!--slide-images-->
        <?php get_template_part('parts/slide','images'); ?>
        <!--/slide-images-->

        <div id="dynamic-content">
            <!--section-services-->
            <?php get_template_part('parts/section','services'); ?>
            <!--/section-services-->

            <!--section-about-us-->
            <?php get_template_part('parts/section','about-us'); ?>
            <!--/section-about-us-->

            <!--section-careers-->
            <?php get_template_part('parts/section','careers'); ?>
            <!--/section-careers-->


            <!--section-worldwide-->
            <?php get_template_part('parts/section','worldwide'); ?>
            <!--/section-worldwide-->

            <!--section-support-->
            <?php get_template_part('parts/section','support'); ?>
            <!--/section-support-->

            <!--section-contact-->
            <?php get_template_part('parts/section','contact'); ?>
            <!--/section-contact-->
        </div>

        <button class="back-top">
            <i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
        </button>
    </div>
    <?php get_footer() ?>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="lib/jquery/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<!--<script src="js/slick.min.js"></script>-->

<script src="lib/revslider/js/jquery.themepunch.tools.min.js"></script>
<script src="lib/revslider/js/jquery.themepunch.revolution.min.js"></script>


<script src="lib/background-effect-move/jquery.interactive_bg.js"></script>

<script src="lib/YTPlayer/jquery.mb.YTPlayer.js"></script>

<script src="js/dropdown-menu-extend.js"></script>
<script src="js/back-top.js"></script>

<script src="js/video-config.js"></script>

<script>
    /*
     $('.owl-carousel').owlCarousel({
     autoPlay: 3000, //Set AutoPlay to 3 seconds

     items : 4,
     itemsDesktop : [1199,3],
     itemsDesktopSmall : [979,3]
     });
     */
</script>

<script type="text/javascript">


    jQuery(document).ready(function () {
        jQuery("#home-slider").show().revolution({
            sliderType: "standard",
            //sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                mouseScrollReverse: "default",
                onHoverStop: "off",
                arrows: {
                    style: "uranus",
                    enable: true,
                    hide_onmobile: false,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    }
                }
            },
            gridwidth: 1024,
            gridheight: 370,
            lazyType: "none",
            shadow: 0,
            spinner: "spinner3",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,

        });
    });
    /*ready*/
</script>


<script>
    $(".worldwide").interactive_bg();

    $(window).resize(function() {
        $(".worldwide > .ibg-bg").css({
            width: $(window).outerWidth(),
            height: $(window).outerHeight()
        })
    })

</script>


<script>
    $(function() {

        var $window  = $(window);
        $window.on('scroll', revealOnScroll);

        function revealOnScroll() {
            var scrolled = $window.scrollTop(),
                win_height_padded = $window.height() * 1.1;

            // Showed...
            $(".wow:not(.animated)").each(function () {
                var $this     = $(this),
                    offsetTop = $this.offset().top;

                if (scrolled + win_height_padded > offsetTop) {
                    if ($this.data('timeout')) {
                        window.setTimeout(function(){
                            $this.addClass('animated ' + $this.data('animation'));
                        }, parseInt($this.data('timeout'),10));
                    } else {
                        $this.addClass('animated ' + $this.data('animation'));
                    }
                }
            });
            // Hidden...

            /*
             //Get list effect name
             var effectList = [];
             $('.wow').each(function () {
             var effect = $(this).data('animation');
             if(effectList.indexOf(effect) == -1)
             {
             effectList.push(effect);
             }
             });

             $(".wow.animated").each(function (index) {
             var $this     = $(this),
             offsetTop = $this.offset().top;
             if (scrolled + win_height_padded < offsetTop)
             {
             $(this).removeClass('animated '+effectList.join(' '));
             }
             });*/
        }
        revealOnScroll();
    });
</script>

</body>
</html>