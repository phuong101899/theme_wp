<div id="nav-menu">
    <nav class="navbar navbar-landing">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse-top" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <!--<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->

                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <a href="#">
                    <img class="img-responsive" src="images/logo/logo.png">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse-top">

                <ul class="nav navbar-nav navbar-right nav-effect">
                    <li class="active"><a href="#">HOME</a></li>
                    <li><a href="#about-us">ABOUT SETDAT GROUP</a></li>
                    <li class="dropdown landing-sub mega-menu">
                        <a href="#">SERVICES</a>
                        <!--
                        <ul class="dropdown-menu" id="dropdown_menu-service-1">
                            <li>
                                <a href="#">E-Commerce & Drop Shipping Services</a>
                            </li>
                            <li><a href="#">Retail Distribution Services</a></li>
                            <li><a href="#">Transportation Management Services</a></li>
                            <li><a href="#">Custom Brokerage and Inbound Forwarding</a></li>
                            <li><a href="#">B2B E-Commerce Platform</a></li>
                            <li><a href="#">Canadian Master Distribution</a></li>
                            <li><a href="#">Staffing Services</a></li>
                            <li><a href="#">BPO Services</a></li>
                            <li><a href="#">IT Services</a></li>
                            <li><a href="#">Customize Fashion House</a></li>
                        </ul>
                        -->

                    </li>
                    <li><a href="#careers">CAREERS</a></li>
                    <li><a href="#contact">CONTACT US</a></li>

                </ul>
            </div><!-- /.navbar-collapse -->

            <div class="dropdown-menu-extend" id="dropdown_menu-service-2">
                <div class="landing-sub">
                    <ul class="dropdown-menu" style="position: relative;background: transparent">
                        <li class="dropdown-menu-sub open">
                            <a href="#">E-Commerce & Drop Shipping Services</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>E-Commerce & Drop Shipping Services</h2>
                                        <p>
                                            Our specially designed e-commerce area will designate locations specifically designed for your product and account.
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/ecommerce.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Retail Distribution Services</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Retail Distribution Services</h2>
                                        <p>
                                            Seldat is at the forefront of distribution management for retail/chain stores. As a leader in the Logistics and Fulfillment industry
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/retail.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Transportation Management Services</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Transportation Management Services</h2>
                                        <p>
                                            Our main objective is to provide our clients with the highest quality service while providing cost savings and superior service.
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/transport-1024x741.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Custom Brokerage and Inbound Forwarding</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Custom Brokerage and Inbound Forwarding</h2>
                                        <p>
                                            US Customs Brokerage As a full service company, we realize the importance of the costs involved in US Customs duties...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/05/customs-brokerage-and-in-bound-forwarding.png">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">B2B E-Commerce Platform</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>B2B E-Commerce Platform</h2>
                                        <p>
                                            Seldat Direct has developed a unique software platform that matches suppliers with retailers who need the product sold by the supplier...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/b2b-ecommerce.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Canadian Master Distribution</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Canadian Master Distribution</h2>
                                        <p>
                                            When it comes to selling product in Canada, most Companies try to do it themselves...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/canada.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Staffing Services</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Staffing Services</h2>
                                        <p>
                                            Seldat Staffing Services is recognized as a provider of outstanding personalized service to both clients and applicants...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/staff-service.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub"><a href="#">BPO Services</a></li>
                        <li class="dropdown-menu-sub">
                            <a href="#">IT Services</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>IT Services</h2>
                                        <p>
                                            When big companies have technology issues, they call their IT department...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/02/it-service.jpg">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-menu-sub">
                            <a href="#">Customize Fashion House</a>
                            <div class="menu-detail">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-1" align="center">
                                        <h2>Customize Fashion House</h2>
                                        <p>
                                            Customize Inc. will be revolutionizing the custom clothing industry. Using a unique combination of technology and Seldat resources...
                                        </p>
                                        <div>
                                            <button class="btn btn-primary btn-lg btn-effect">BOOK NOW</button>
                                            <button class="btn btn-default btn-lg">READ MORE</button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <img class="img-responsive img-thumbnail" src="http://seldatinc.com/wp-content/uploads/2016/05/customize-company-description.png">
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>
</div>
