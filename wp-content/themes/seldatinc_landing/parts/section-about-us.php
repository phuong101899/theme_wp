<section id="about-us">
    <div class="landing-title wow" data-animation="bounceInLeft">
        <span class="landing-line "></span><span>about us</span><span class="landing-line"></span>
    </div>
    <div class="container-content wow" data-animation="bounceInRight" data-timeout="200">
        Our expertise combined with our great flexibility enables us to support and accelerate our partners’ growth, because they can spend more time focusing on their core business...
        <a href="#" class="read-more">Read more</a>
    </div>
    <div class="video-bg">
        <div class="embed-responsive" style="height: 500px" id="show-video-bg"></div>
        <div class="video-container">
            <button class="play-btn"><i class="fa fa-play-circle-o" aria-hidden="true"></i>
            </button>
        </div>

    </div>
</section>