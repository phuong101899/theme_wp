<section id="careers">
    <div class="landing-title wow" data-animation="flipInX" data-timeout="200">
        <span class="landing-line"></span><span>seldat careers</span><span class="landing-line"></span>
    </div>
    <div class="container-content wow" data-animation="flipInX" data-timeout="200">
        With over 10 years in the logistics industry,
        Seldat Inc is specialized in providing 3PL services related to order fulfillment (B2B),
        pick & pack, e-commerce logistics (B2C), warehousing and distribution as well as value added
        services.
    </div>
    <div align="center">
        <button class="btn btn-primary btn-lg read-more btn-effect wow" data-animation="flipInY" data-timeout="400">read more</button>
    </div>
    <div class="parallax-section" style="background-image: url('images/parallax/mission.png');margin-top: 50px">
        <div class="parallax-modal">
            <div class="container wow" data-animation="bounceIn">
                <div class="border-length">
                    <h1>SELDAT'S MISSION IS CLEAR</h1>
                    <p>
                        Provice quality services through our experienced service team by providing
                        technological advantages at the most competitive rates
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>