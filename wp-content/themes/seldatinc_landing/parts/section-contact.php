<section id="contact">
    <div class="landing-title wow"  data-animation="zoomIn" data-timeout="200">
        <span class="landing-line"></span><span>contact us</span><span class="landing-line"></span>
    </div>
    <div class="container-content">
        <p class="wow" data-animation="fadeInDown" data-timeout="200">
            We'd really love to hear from you so why not drop us an email and we'll get back to you as soon as we can
        </p>
        <form>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input class="form-control input-lg wow" placeholder="Full name" data-animation="flipInX">
                    </div>
                    <div class="form-group">
                        <input class="form-control input-lg wow" placeholder="Email" data-animation="flipInX">
                    </div>
                    <div class="form-group">
                        <input class="form-control input-lg wow" placeholder="Subject" data-animation="flipInX">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group wow" data-animation="flipInX">
                        <textarea style="margin-top: 0px;margin-bottom: 0px;height: 107px;resize: none;" class="form-control input-lg" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group wow" data-animation="flipInY">
                        <button class="btn btn-primary btn-block btn-effect" style="height: 46px;margin-top: -1px;margin-left: 0">
                            <!--<i class="glyphicon glyphicon-envelope"></i>-->
                            <img src="images/icons/icon_mail.png">
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>