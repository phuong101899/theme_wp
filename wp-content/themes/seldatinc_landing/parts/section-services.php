<section>
    <div class="landing-title wow" data-animation="bounceIn">
        <span class="landing-line"></span><span>our services</span><span class="landing-line"></span>
    </div>
    <div id="landing-services">
        <div class="float_center">
            <div class="child">
                <div class="item">
                    <div class="flipper">
                        <div class="front">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_1.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                <div class="item-divider"></div>
                                <div class="item-content-detail">Our retail locations are an oasisfor our customers...</div>
                            </div>
                        </div>
                        <div class="back">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_1_1.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="content-wrap">
                                    <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                    <div class="item-content-readmore">
                                        <div class="btn btn-default item-link"><a href="#">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="flipper">
                        <div class="front">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_2.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                <div class="item-divider"></div>
                                <div class="item-content-detail">Our retail locations are an oasisfor our customers...</div>
                            </div>
                        </div>
                        <div class="back">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_2_2.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="content-wrap">
                                    <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                    <div class="item-content-readmore">
                                        <div class="btn btn-default item-link"><a href="#">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="flipper">
                        <div class="front">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_3.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                <div class="item-divider"></div>
                                <div class="item-content-detail">Our retail locations are an oasisfor our customers...</div>
                            </div>
                        </div>
                        <div class="back">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_3_3.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="content-wrap">
                                    <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                    <div class="item-content-readmore">
                                        <div class="btn btn-default item-link"><a href="#">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="flipper">
                        <div class="front">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_4.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                <div class="item-divider"></div>
                                <div class="item-content-detail">Our retail locations are an oasisfor our customers...</div>
                            </div>
                        </div>
                        <div class="back">
                            <div class="item-header">
                                <div class="item-img">
                                    <img class="img-circle"
                                         src="images/banner/img_4_4.jpg"
                                         alt="Generic placeholder image">
                                </div>
                            </div>
                            <div class="item-content">
                                <div class="content-wrap">
                                    <h4 class="item-title-text">CUSTOMIZE INC. COMPANY DESCRIPTION</h4>
                                    <div class="item-content-readmore">
                                        <div class="btn btn-default item-link"><a href="#">Read more</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</section>