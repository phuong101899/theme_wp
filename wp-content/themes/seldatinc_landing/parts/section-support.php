<section class="parallax-section" style="background-image: url('images/parallax/parallax4.jpg');">
    <div class="parallax-modal">
        <div class="container">
            <ul  class="wow" data-animation="bounceIn" data-timeout="200">
                <li>
                    <span class="parallax-text" >SUPPORT@SELDATINC.COM</span>
                </li>
                <li>
                    <span class="parallax-text text-boxed">+1-732-348-0000</span>
                </li>
                <li>
                    <span class="parallax-text " >1900 RIVER ROAD,</span>
                </li>
                <li>
                    <span class="parallax-text" >BURLINGTON NJ, UNITED STATES</span>
                </li>
            </ul>
        </div>
    </div>
</section>