<div class="worldwide" data-ibg-bg="images/parallax/map.png">
    <div class="main">
        <div class="landing-title wow" data-animation="slideInDown" data-timeout="200">
            <span class="landing-line"></span><span>seldat worldwide</span><span class="landing-line"></span>
        </div>
        <div class="container-content">
            <p data-animation="slideInUp" data-timeout="400" class="wow">
                With over 10 years in the logistics industry,
                Seldat Inc is specialized in providing 3PL services related to order fulfillment (B2B),
                pick & pack, e-commerce logistics (B2C), warehousing and distribution as well as value added
                services.
            </p>

        </div>
        <div align="center" class="wow"  data-animation="zoomIn" data-timeout="400">
            <button class="btn btn-primary btn-lg read-more btn-effect">read more</button>
        </div>
    </div>

</div>