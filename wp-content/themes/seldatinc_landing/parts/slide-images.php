<div id="slide-images">
    <div>

        <div class="rev_slider_wrapper fullwidthbanner-container">
            <!-- START REVOLUTION SLIDER 5.2.5.4 fullwidth mode -->
            <div id="home-slider" class="rev_slider fullwidthabanner" style="display:none;"
                 data-version="5.2.5.4">
                <ul>    <!-- SLIDE  -->
                    <li data-index="rs-1" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                        data-easeout="default" data-masterspeed="300"
                        data-thumb="images/banner/banner_01.jpg"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                        data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                        data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner/banner_01.jpg" alt=""
                             title="Slide1" width="2050" height="600" data-bgposition="center center"
                             data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone"
                             data-scalestart="100" data-scaleend="115" data-rotatestart="0" data-rotateend="0"
                             data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <h1 class="tp-caption"
                            data-x="right"
                            data-y="center" data-voffset="-25"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"

                            data-transform_in="opacity:0;s:300;e:Power2.easeInOut;"
                            data-transform_out="opacity:0;s:300;"
                            data-start="500"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on">Explore New horizons! </h1>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption"
                             data-x="right"
                             data-y="center" data-voffset="35"
                             data-width="['auto']"
                             data-height="['auto']"
                             data-visibility="['on','on','on','off']"
                             data-transform_idle="o:1;"

                             data-transform_in="x:right;s:900;e:Power2.easeInOut;"
                             data-transform_out="opacity:0;s:1.875;"
                             data-start="500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing
                            <br/> elit. Praesent vestibulum molestie lacus! </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-9" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                        data-easeout="default" data-masterspeed="300"
                        data-thumb="images/banner/banner_02.jpg"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                        data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                        data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner/banner_02.jpg"
                             alt="" title="slide-2" width="2050" height="600" data-bgposition="center center"
                             data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone"
                             data-scalestart="115" data-scaleend="100" data-rotatestart="0" data-rotateend="0"
                             data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>

                        <!-- LAYER NR. 1 -->
                        <h1 class="tp-caption"
                            data-x="left"
                            data-y="center" data-voffset="-25"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"

                            data-transform_in="opacity:0;s:300;e:Power2.easeInOut;"
                            data-transform_out="opacity:0;s:300;"
                            data-start="500"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on">Explore New horizons! </h1>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption Fashion-BigDisplay   tp-resizeme  full-size"
                             data-x="left"
                             data-y="center" data-voffset="35"
                             data-width="['auto']"
                             data-height="['auto']"
                             data-visibility="['on','on','on','off']"
                             data-transform_idle="o:1;"

                             data-transform_in="x:left;s:900;e:Power2.easeInOut;"
                             data-transform_out="opacity:0;s:1.875;"
                             data-start="500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing
                            <br/> elit. Praesent vestibulum molestie lacus! </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-10" data-transition="fade" data-slotamount="default"
                        data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default"
                        data-easeout="default" data-masterspeed="300"
                        data-thumb="images/banner/banner_03.jpg"
                        data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                        data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                        data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/banner/banner_03.jpg"
                             alt="" title="slide-3" width="2050" height="600" data-bgposition="center center"
                             data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone"
                             data-scalestart="100" data-scaleend="115" data-rotatestart="0" data-rotateend="0"
                             data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <h1 class="tp-caption"
                            data-x="left"
                            data-y="center" data-voffset="-25"
                            data-width="['auto']"
                            data-height="['auto']"
                            data-transform_idle="o:1;"

                            data-transform_in="opacity:0;s:300;e:Power2.easeInOut;"
                            data-transform_out="opacity:0;s:300;"
                            data-start="500"
                            data-splitin="none"
                            data-splitout="none"
                            data-responsive_offset="on">Explore New horizons! </h1>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption Fashion-BigDisplay   tp-resizeme  full-size"
                             data-x="left"
                             data-y="center" data-voffset="35"
                             data-width="['auto']"
                             data-height="['auto']"
                             data-visibility="['on','on','on','off']"
                             data-transform_idle="o:1;"

                             data-transform_in="x:left;s:900;e:Power2.easeInOut;"
                             data-transform_out="opacity:0;s:1.875;"
                             data-start="500"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing
                            <br/> elit. Praesent vestibulum molestie lacus! </div>
                    </li>
                </ul>

                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>




        </div><!-- END REVOLUTION SLIDER -->
    </div>
</div>