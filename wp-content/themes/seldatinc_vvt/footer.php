<footer>

    <div class="container">

        <div style="border-bottom: solid 1px #9f9f9f; margin: 20px 0px 10px 0px;"></div>

        <div class="row">

            <div class="col-xs-3 left-page">

                <h3 style="margin-bottom: 0px;margin-top: 14px"><a href="<?php echo home_url().'/'.qtrans_getLanguage()?>">SELDAT</a></h3>



                <p style="margin: 0px">1900 River Road,</p>



                <p>Burlington NJ, United States <br/>+1-732-348-0000</p>

            </div>

            <div class="col-xs-9 content-page" style="width: 768px">

                <?php wp_nav_menu(array('menu'=>'footer-menu-1', 'container'=>'','menu_class'      => 'nav navbar-nav menu_footer')); ?>

		<a class="filePdf" href="<?php echo esc_url(get_template_directory_uri()); ?>/pdf/<?php echo qtrans_getLanguage();?>/SELDAT_BOOKLET.pdf" target="_blank">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-file.png" alt="">
                    <p>
                        <?php 
                        if (qtrans_getLanguage() == 'en')
                            echo "<span>Download</span>Seldat Brochure";
                        elseif (qtrans_getLanguage() == 'vi')
                            echo "<span>Download</span>Seldat Brochure";
                        elseif (qtrans_getLanguage() == 'es')
                            echo "<span>Descargar</span>Seldat Folleto";
                        ?>
                    </p>
                </a>

                <div class="clear"></div>

                <p class="copyright">Copyright &copy; Seldatinc 2015</p>

            </div>

        </div>

    </div>

</footer>

<?php wp_footer(); ?>

</body>

</html>

