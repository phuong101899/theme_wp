<?php
/*
Template Name: Template full-width
*/
get_header();
?>
<div class="container">
    <?php
    // Start the loop.
    while (have_posts()) : the_post();
        the_content();
        // End the loop.
    endwhile;
    ?>
</div>
<?php get_footer(); ?>