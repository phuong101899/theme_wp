<html>

<head>
    <title>SeldatInc</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.css">
    <!-- Optional theme -->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/script.js"></script>
    <link href='https://fonts.googleapis.com/css?family=RobotoDraft:400,500,700,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style.css" />
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/flag-icon.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/font-awesome/css/font-awesome.css" />
</head>

<body <?php body_class(); ?>>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-3"> <a class="logo" href="<?php echo home_url() ?>">
				<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="SeldatInc"/></a> </div>
                <div class="col-xs-9">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="pull-right menu_top_menu">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="    float: left;    line-height: 20px;    display: inline-block;">
                                        <?php if (get_language_attributes() == 'en'): ?>
                                            <div style="  height: 20px; width: 20px; float: left" class="img-thumbnail flag flag-icon-background flag-icon-gb" title="au" id="au"> </div> &nbsp;
                                            <?php endif; ?>
                                                <?php if (get_language_attributes() == 'vi'): ?>
                                                    <div style="  height: 20px; width: 20px; float: left" class="img-thumbnail flag flag-icon-background flag-icon-vn" title="au" id="au"> </div> &nbsp;
                                                    <?php endif; ?>
                                                        <?php if (get_language_attributes() == 'es'): ?>
                                                            <div style="  height: 20px; width: 20px; float: left;" class="img-thumbnail flag flag-icon-background flag-icon-es" title="au" id="au"> </div> &nbsp;
                                                            <?php endif; ?>
                                                                <?php 
                                                                    if (get_language_attributes() == 'en')
                                                                        echo 'English';
                                                                    elseif (get_language_attributes() == 'vi')
                                                                        echo "Vietmamese";
                                                                    elseif (get_language_attributes() == 'es')
                                                                        echo 'Spanish';
                                                                ?>
                                                                <span class="caret"></span> </a>                                                            
                                                                
                                    <ul class="dropdown-menu" role="menu" style="min-width:90px">
                                        <li>
                                            <a href="<?php echo site_url('gb') ?>">
                                                <div class="img-thumbnail flag flag-icon-background flag-icon-gb"></div> English</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('vi') ?>">
                                                <div class="img-thumbnail flag flag-icon-background flag-icon-vn"></div> VietNam</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('es') ?>">
                                                <div class="img-thumbnail flag flag-icon-background flag-icon-es"></div> Spanish</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <form action="<?php echo site_url() ?>" id="form-search" class="form-search">
                                <div class="input-group">
                                    <input type="text" name="s" class="form-control" placeholder="<?php 
										if (get_language_attributes() == 'en')
											echo "Search all information";
										elseif (get_language_attributes() == 'vi')
											echo "Nhập từ khóa cần tìm";
										elseif (get_language_attributes() == 'es')
											echo "Buscar toda la información";
									?> " aria-describedby="basic-addon2"> <span onclick="document.getElementById('form-search').submit();" class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span> </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_head(); ?>
    </header>
    <div class="container">
		
        <?php    wp_nav_menu(array('theme_location' => 'main-menu', 'container_class' => 'navbar-seldat'));    //        include('navigation.php');    ?>
    </div>
	
	<script>
	$(document).ready(function () { updateHomelink(); });
	function updateHomelink()
	{
		$("a[href='/']").attr("href", '/<?php echo get_language_attributes(); ?>');
		
		var pathname = window.location.pathname;
		var newString  = "";
		if(pathname.indexOf("/vi/") > -1) {
			newString = pathname.replace('/vi', '');
		}
		if(pathname.indexOf("/en/") > -1) {
		  newString = pathname.replace('/en', '');
		}
		if(pathname.indexOf("/es/") > -1) {			
			newString = pathname.replace('/es', '');
		}
		$(".dropdown-menu a[href='<?php echo site_url('gb') ?>']").attr("href", '<?php echo site_url(); ?>/en'+newString);
		$(".dropdown-menu a[href='<?php echo site_url('vi') ?>']").attr("href", '<?php echo site_url(); ?>/vi'+newString);
		$(".dropdown-menu a[href='<?php echo site_url('es') ?>']").attr("href", '<?php echo site_url(); ?>/es'+newString);
	}
	</script>

   