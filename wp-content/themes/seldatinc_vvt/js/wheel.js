//(jQuery.noConflict())(function($){
$(document).ready(function(){
	$.fn.WheelSlider = function(options)
	{
		var data = {
				'startPosition': 1,
				'itemPrefix': 'item',
				'itemNumber': 5,
				'prevNextClass': 'navigation',
				'navigationClass': 'wheel-navigator',
				'currentElementClass': 'current',
				'wheelContentClass': 'wheel-content',
				'imagesContainerClass': 'wheel-image',
				'wheelElementContainerID': 'wheel-holder'
			},
			startPosition,
			itemPrefix,
			itemNumber,
			prevNextClass,
			navigationClass,
			navigation,
			currentElementClass,
			wheelContentClass,
			wheelContent,
			imagesContainerClass,
			imagesContainer,
			currentPosition,
			contentWidth,
			angle = 0,
			wheelElementContainerID,
			wheelElement,
			wheelElementContainer,

			container = this,

			methods = {
				init: function(){
					if (options){
		                $.extend(data, options);
		            }
					startPosition = currentPosition = data['startPosition'];
					itemPrefix = data['itemPrefix'];
					itemNumber = data['itemNumber'];
					prevNextClass = data['prevNextClass'];
					navigationClass = data['navigationClass'];
					currentElementClass = data['currentElementClass'];
					wheelContentClass = data['wheelContentClass'];
					imagesContainerClass = data['imagesContainerClass'];
					wheelElementContainerID = data['wheelElementContainerID'];

					wheelElementContainer = $('#'+wheelElementContainerID);
					wheelElement = $('IMG', wheelElementContainer);
					navigation = $('.'+navigationClass);
					wheelContent = $('.'+wheelContentClass);
					imagesContainer = $('.'+imagesContainerClass);

					contentWidth = $('.item', wheelContent).width();

					methods.initTransparency();
					methods.initRotation();
					methods.initActions();
					methods.goToSlide(startPosition);
				},

				initActions: function() {
					//click on navigation dots
					$('a', navigation).click(function(){
						methods.goToSlide(this.hash.replace('#', ''), false);
						return false;
					});

					//click on prev / next navigation
					$('a.navigation', container).click(function(){
						switch(this.hash)
						{
							case '#next':
								methods.goToNext();
								break;
							case '#prev':
								methods.goToPrev();
								break;
						}
						return false;
					});

					//hover on prev, next
					if(!prototypes.ltIE8())
					{
						$('a.navigation', container).hover(
								function(){
									$(this).stop().animate({'opacity': 0.9}, 250);
								},
								function(){
									$(this).stop().animate({'opacity': 1}, 250);
								}
						);
					}

					/* How it Work (ajax content)
					================================================== */
					if($('.ajaxified #menu-how-it-works, .ajaxified #menu-comment-ca-marche').length)
					{
						//$("#menu-how-it-works a").click(function(){
						$("#menu-how-it-works a, #menu-comment-ca-marche a").bind('click', function(){
							var pos = Number($('#menu-how-it-works > li a, #menu-comment-ca-marche > li a').index(this))+1;
							methods.goToSlide(pos);

							return false;
						});

						$('<DIV />', {
							'id': 'ajax-loading'
						}).insertAfter($('#ajax-content'))
						.ajaxStart(function(){
							$('#ajax-loading').css({'width': $('#ajax-content').width()+'px', 'height': $('#ajax-content').height()+'px'}).stop().fadeTo(200, 0.8);
						});
					}
				},

				initRotation: function(){

					wheelElement.one('load', function() {
							var src = wheelElement.attr('src');
							//get image information
							var height = this.height;
							var width = this.width;
							var left = $(this).offset().left - $(this).parent().offset().left;
							var top = $(this).offset().top - $(this).parent().offset().top;

							//clear container
							wheelElementContainer.html('');

							//replace by svg
							var R = Raphael(wheelElementContainerID, wheelElementContainer.width(), wheelElementContainer.height());
							wheelElement = R.image(src, left, top, width, height);

							var wheelButtons = $("<DIV/>", {
								'id': 'wheelButtons',
								'style': 'position: absolute; right: 14px; bottom: 18px; z-index: 3;'
							}).insertAfter('.wheel-image');
							var R2 = Raphael('wheelButtons', wheelElementContainer.width(), wheelElementContainer.height());
							var buttons = {
								item1: R2.path("M340.0,43.0c-15.672-9.596-32.585-16.611-50.301-20.856c-17.622-4.223-35.756-5.649-53.898-4.238c-17.934,1.396-35.479,5.529-52.149,12.284c-14.474,5.867-28.009,13.599-40.334,23.035l35.437,5.662c0.473,0.075,0.91,0.243,1.295,0.483c1.004,0.628,1.652,1.75,1.636,2.999l-0.476,36.499c37.299-27.431,87.408-29.843,127.375-5.635l37.475-11.637L340.0,43.0z").attr({stroke: "none", fill: "#AA0E14", opacity: 0, cursor: 'pointer'}),
								item2: R2.path("M415.0,235.0c4.285-17.87,5.729-36.125,4.292-54.284c-1.428-18.064-5.677-35.752-12.625-52.57c-6.869-16.625-16.221-32.034-27.797-45.8c-10.052-11.954-21.589-22.436-34.372-31.243l5.566,35.453c0.074,0.472,0.049,0.94-0.061,1.38c-0.286,1.149-1.151,2.112-2.346,2.482l-34.859,10.826c37.615,26.998,55.394,73.908,44.72,119.399l22.65,32.046L415.0,235.0z").attr({stroke: "none", fill: "#FEBE10", opacity: 0, cursor: 'pointer'}),
								item3: R2.path("M260.0,370.0c18.318-1.448,36.126-5.716,52.953-12.692c16.739-6.942,32.248-16.447,46.096-28.254c13.688-11.669,25.454-25.325,34.97-40.59c8.262-13.253,14.665-27.465,19.091-42.344l-31.998,16.25c-0.426,0.216-0.877,0.337-1.331,0.369c-1.182,0.082-2.364-0.443-3.086-1.463l-21.068-29.81c-14.053,44.117-53.173,75.521-99.736,79.429l-23.478,31.442L260.0,370.0z").attr({stroke: "none", fill: "#81AE41", opacity: 0, cursor: 'pointer'}),
								item4: R2.path("M80.0,260.0c7.038,16.975,16.598,32.593,28.434,46.439c11.774,13.775,25.607,25.588,41.114,35.11c15.329,9.411,31.953,16.381,49.41,20.714c15.157,3.762,30.653,5.461,46.172,5.073l-25.343-25.411c-0.336-0.338-0.591-0.731-0.763-1.152c-0.443-1.099-0.309-2.386,0.439-3.387l21.838-29.25c-46.298,0.27-88.255-27.232-106.36-70.31l-37.16-12.612L80.0,260.0z").attr({stroke: "none", fill: "#007DAC", opacity: 0, cursor: 'pointer'}),
								item5: R2.path("M132.0,58.0c-13.969,11.938-25.869,25.857-35.38,41.392c-9.462,15.455-16.422,32.261-20.686,49.951c-4.215,17.487-5.707,35.451-4.433,53.393c1.106,15.578,4.278,30.84,9.444,45.479l16.334-31.954c0.218-0.425,0.512-0.789,0.861-1.08c0.908-0.762,2.174-1.032,3.357-0.63l34.565,11.73c-14.563-43.95-1.371-92.353,34.002-122.882l0.512-39.238L132.0,58.0z").attr({stroke: "none", fill: "#4A3D6F", opacity: 0, cursor: 'pointer'})
							};

							buttons.item1.click(function(){
								//do nothing, current position
							});

							buttons.item2.click(function(){
								methods.goToSlide(currentPosition+1);
							});

							buttons.item3.click(function(){
								methods.goToSlide(currentPosition+2);
							});

							buttons.item4.click(function(){
								methods.goToSlide(currentPosition+3);
							});

							buttons.item5.click(function(){
								methods.goToSlide(currentPosition+4);
							});

					}).each(function() {
						if(this.complete) $(this).load();
					});
				},

				initTransparency: function(){
					if(prototypes.ltIE8())
					{
						$('IMG', imagesContainer).hide();
						$('IMG', imagesContainer).css({'top': '0px'});
					}
					else
					{
						$('IMG', imagesContainer).css({'opacity': 0, 'top': '0px'})
					}
				},

				goToNext: function(){
					methods.goToSlide(currentPosition+1, false);
				},

				goToPrev: function(){
					methods.goToSlide(currentPosition-1, true);
				},

				goToSlide: function(position, reverse){
					position = (Number(position)%itemNumber);
					if(Number(position) <= 0)
					{
						position = itemNumber;
					}

					methods.turnTheWheel(currentPosition, Number(position), reverse);
					currentPosition = Number(position);

					methods.setBottomContent(currentPosition);

					var itemId = itemPrefix+currentPosition;

					var visibleItems = $('.item:visible', wheelContent);
					if(visibleItems.length)
					{
						visibleItems.stop().animate({'opacity': 0, 'left': (-contentWidth)+'px'}, 250, function(){
							$('#'+itemId).stop().animate({'opacity': 1, 'left': '30px'}, 250);
						});
					}
					else
					{
						$('#'+itemId).stop().animate({'opacity': 1, 'left': '30px'}, 250)
					}

					//set navigation item
					navigation.find('a').removeClass(currentElementClass);
					navigation.find("a[href='#"+currentPosition+"']").addClass(currentElementClass);

					methods.displayImage(itemId);
				},

				turnTheWheel: function(from, to, reverse){
					if(reverse)
					{
						var tmp = to;
						to = from;
						from = tmp;
					}

					if((from > to))
					{
						to += itemNumber;
					}

					var mult = reverse ? 1 : -1;
					angle += (to-from)*72*mult;

					wheelElement.stop().animate({transform: "r" + angle}, 500);
				},

				displayImage: function(itemId) {
					//crossfade images visibles
					var from = $('IMG:visible', imagesContainer);
					var to = $('#'+itemId+'-image');

					if(prototypes.ltIE8()) //ie8- cannot animate transparency
					{
						from.hide();
						to.show();
					}
					else
					{
						from.stop().animate({'opacity': 0}, 300);
						to.stop().animate({'opacity': 1}, 300);
					}
				},

				setBottomContent: function(position) {
					var current_menu_item = $('#menu-how-it-works > LI:nth-child('+position+') a, #menu-comment-ca-marche > LI:nth-child('+position+') a');
					var url_page = current_menu_item.attr('href')+'?content=true';
					current_menu_item.parents('UL').find('LI').removeClass('current-menu-item');
					$.ajax({
						url: url_page,
						type: 'GET',
						async: true,
						success: function(data) {
						$('#ajax-content').stop().animate({'opacity': 0}, 300, function(){
								$(this).html(data)
								$(this).animate({'opacity': 1}, 300, function(){
									$('#ajax-loading').stop().fadeOut(200);
								});
							});
						}
					});
					current_menu_item.parent('LI').addClass('current-menu-item');
				}
			},

			prototypes = {
				ltIE8: function(){
					return ($.browser.msie && ($.browser.version < 9));
				},

		        isTouchDevice:function(){// Detect Touchscreen devices
		            var isTouch = false,
		            agent = navigator.userAgent.toLowerCase();

		            if (agent.indexOf('android') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('blackberry') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('ipad') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('iphone') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('ipod') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('palm') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('series60') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('symbian') != -1){
		                isTouch = true;
		            }
		            if (agent.indexOf('windows ce') != -1){
		                isTouch = true;
		            }

		            return isTouch;
		        }
			};

		return methods.init.apply(this);
	}

	//entry point
	$('.wheel-container').WheelSlider({
		'startPosition': 1,
		'itemPrefix': 'item',
		'itemNumber': 5,
		'prevNextClass': 'navigation',
		'navigationClass': 'wheel-navigator',
		'currentElementClass': 'current',
		'wheelContentClass': 'wheel-content',
		'wheelElementContainerID': 'wheel-holder'
	});
});