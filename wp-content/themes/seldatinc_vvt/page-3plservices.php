﻿<?php
/*
Template Name: Template 3PL Service
*/
get_header();
?>
<link rel='stylesheet' id='wheel-css'
      href='<?php echo esc_url(get_template_directory_uri()); ?>/css/wheel.css' type='text/css'
      media='all'/>
<script type='text/javascript' src='http://code.jquery.com/jquery-1.7.1.min.js'></script>
<script type='text/javascript'
        src='<?php echo esc_url(get_template_directory_uri()); ?>/js/raphael-min.js'></script>
<script type='text/javascript'
        src='<?php echo esc_url(get_template_directory_uri()); ?>/js/wheel.js'></script>

<div class="container">


    <div class="row">
        <div class="col-xs-3 left-page">
            <div class="box-sel">
                <h4 class="title"><?php 
					if (qtrans_getLanguage() == 'en')
						echo 'Special Services';
					elseif (qtrans_getLanguage() == 'vi')
						echo "Những Dịch Vụ Đặc Biệt";
					elseif (qtrans_getLanguage() == 'es')
						echo 'Servicios Especiales';
				?></h4>
                <?php
                wp_nav_menu(array('theme_location' => 'left-menu', 'container_class' => 'seldat_left_menu_class'));
                ?>
            </div>
        </div>
        <div class="col-xs-6 content-page">
            <div class="content">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                the_title('<h3 class="entry-title">', '</h3>');
                ?>
                <div class="clear"></div>
                <div class="wheel-container">
                    <a href="#prev" class="navigation prev"></a>
                    <a href="#next" class="navigation next"></a>

                    <div class="wheel-content">
                        <div id="item1" class="item">
                            <h1 class="sub-title-content">
                                <?php
                                if (qtrans_getLanguage() == 'en'){
									echo '01. SHIP TO SELDAT';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "01. CHUYỂN HÀNG HÓA ĐẾN SELDAT";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '01. ENVIO A SELDAT';
								}                                
                                ?>
                            </h1>
                            <strong>
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo 'Either you want to manufacture your own products or order them from various suppliers around the globe; you simply need to send your goods directly to one of our facilities. No need to worry about space availability and receiving capacity, Wiptec offers scalable solutions that will sustain your peaks and growth..';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo '
									Dù bạn muốn tự sản xuất hàng hóa cho riêng mình hay đặt hàng hóa từ các nhà cung cấp khác nhau trên toàn cầu, một cách đơn giản, bạn chỉ cần gửi hàng hóa trực tiếp đến một trong những cơ sở cũng chúng tôi. Bạn không cần phải lo lắng về khả năng chưa hàng và nhận hàng, Wiptec cung cấp những giải pháp mở rộng sẽ đảm bảo duy trì được trong giai đoạn cao điểm và sự tăng trưởng của bạn.
									';
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '
									Ya sea que usted quiere fabricar sus propios productos o pedirlos de diversos proveedores en todo el mundo; sólo tiene que enviar su mercancía directamente a una de nuestras instalaciones. No hay necesidad de preocuparse por la capacidad de espacio o recibo. Wiptec ofrece soluciones escalables que sostendrán sus picos y crecimiento..
									';
								}                                
                                ?>
							
							
							</strong></div>
                        <div id="item2" class="item">
                            <h1 class="sub-title-content">
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo '02. RECEIVE AND INSPECT STOCK';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "02. NHẬN VÀ KIỂM TRA HÀNG HÓA";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '02. RECIBIR Y CONTROLAR EL STOCK';
								}                                
							?>
							</h1>
                            <strong>
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo 'We receive, count, inspect and stock your products in our distribution centers. All
                                received
                                items are verified against the quantities reported in your receiving order (ASN).
                                Quantity and
                                quality discrepancies are highlighted and product (SKU) dimensions and weights are
                                checked
                                electronically.';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo '
									Chúng tôi nhận hàng, kiểm đếm, kiểm tra và vận chuyển hàng hóa của bạn đến các trung tâm phân phối của chúng tôi. Tất cả các mục hàng hóa chúng tôi nhận sẽ được phân loại theo số lượng như đã được ghi nhận trên biên bản nhận hàng của bạn (ASN). Bất cứ sự không đồng nhất nào về số lượng và chất lượng sẽ được làm nổi bật lên, kích thước và cân nặng của hàng hóa được kiểm tra một cách điện tử. 
									';
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '
									Recibimos, contamos, inspeccionamos y almacenamos sus productos en nuestros centros de distribución. Todos los artículos recibidos se verifican con las cantidades reportadas en su orden de recepción (ASN).  Las  discrepancias de cantidad y/o calidad se resaltan y las dimensiones y peso del producto (SKU) son verificados electrónicamente.
									';
								}                                
							?>
							</strong></div>
                        <div id="item3" class="item">
                            <h1 class="sub-title-content">
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo '03. YOU SELL PRODUCTS';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "03. BẠN BÁN HÀNG HÓA";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '03. USTED VENDE PRODUCTOS';
								}                                
							?>
							</h1>
                            <strong>
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo 'Orders are entered in your web store, accounting or ERP system and instantly
                                transmitted to
                                our system. We take out the pain that comes from managing multi-channel and
                                cross-channel sales
                                by centralizing all your orders in our powerful yet flexible order management system
                                which is
                                part of our LEAD system.';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "
									Các đơn hàng sẽ đi vào trang web cửa hàng của bạn, hệ thống kế toán hay hệ thống ERP và ngay lập tức thông tin được chuyển đến hệ thống của chúng tôi. Chúng tôi giải quyết các vấn đề có nguồn gốc từ việc quản lý bán hàng đa kênh và chéo kênh bằng cách tập trung tất cả đơn hàng của bạn trong một hệ thống quản lý đơn hàng hiệu quả và linh hoạt của chúng tôi, nó là một phần của hệ thống LEAD của công ty.
									";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '
									Los pedidos se introducen en su tienda web, contabilidad o sistema ERP y al instante se transmiten a nuestro sistema. Eliminamos el malestar que causa manejar múltiples canales de venta mediante la centralización de todos sus pedidos en nuestro conveniente y flexible sistema de gestión de pedidos LEAD.
									';
								}                                
							?>
							</strong></div>
                        <div id="item4" class="item">
                            <h1 class="sub-title-content">
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo '04. PROCESS, PACK &AMP; SHIP';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "04. XỬ LÝ ĐƠN HÀNG, ĐÓNG GÓI VÀ CHUYỂN HÀNG";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '04. PROCESO, PACK & SHIP';
								}                                
							?>
							</h1>
                            <strong>
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo 'We use cutting edge technology and equipment to quickly fulfill your orders and ship
                                perfect
                                orders every time. Once the orders are in our LEAD system, we use automated RF picking
                                and
                                barcode scanning to pick, check, pack and ship your orders.';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "
									Chúng tôi xử dụng công nghệ và trang thiết bị vượt trội để nhanh chóng đáp ứng các đơn hàng của bạn và vận chuyến các đơn hàng hoàn hảo bất kỳ lúc nào. Một khi các đơn hàng đã được lưu vào hệ thống LEAD của chúng tôi, chúng tôi sẽ sử dụng RF để lấy hàng từ động, quét mã vạch để lấy hàng, kiểm tra, đóng gói và vận chuyển đơn hàng của bạn.
									";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '
									Utilizamos lo último en  tecnología y equipo para satisfacer rápidamente sus pedidos y enviar cada pedido perfecto. Una vez que las órdenes están en nuestro sistema LEAD, utilizamos picking RF automatizado y escaneo de código de barras para recoger, comprobar, empacar y enviar sus órdenes.
									';
								}                                
							?>
							</strong></div>
                        <div id="item5" class="item">
                            <h1 class="sub-title-content">
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo '05. SATISFY CUSTOMER &AMP; SELL AGAIN';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "05. LÀM HÀI LÒNG KHÁCH HÀNG VÀ BÁN HÀNG LẠI";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '05. SATISFACER AL CLIENTE Y VENDER OTRA VEZ';
								}                                
							?>
							</h1>
                            <strong>
							<?php
                                if (qtrans_getLanguage() == 'en'){
									echo 'Your customers receive their orders completed and on time, every time! Gain a
                                competitive
                                edge in your market by giving your customers the highest level of service in the
                                fulfillment and
                                delivery of their orders.';
								}
                                elseif (qtrans_getLanguage() == 'vi') {
									echo "Khách hàng của bạn sẽ nhận được hàng hóa đầy đủ như trong đơn hàng và đúng thời gian, vào bất cứ lúc nào. Đạt được lợi thế cạnh tranh trong thị trường bạn đang hoạt động bằng cách cung cấp cho khách hàng của bạn dịch vụ cao nhất trong việc hoàn chỉnh đơn hàng và giao hàng tận nơi.";
								}
                                elseif (qtrans_getLanguage() == 'es') {
									echo '
									Sus clientes reciben sus pedidos completados y a tiempo, siempre! Obtenga una ventaja competitiva en su mercado, dando a sus clientes el más alto nivel de servicio en el cumplimiento y la entrega de sus pedidos.
									';
								}                                
							?>
							</strong></div>
                    </div>
                    <div class="wheel-navigator">
                        <a href="#1" title="Ship to Wiptec"></a><a href="#2" title="Receive and Inspect Stock"></a><a
                            href="#3"
                            title="You Sell Products"></a><a
                            href="#4" title="Process, Pack &#038; Ship"></a><a href="#5"
                                                                               title="Satisfy Customer &#038; Sell Again"></a>
                    </div>

                    <div id="wheel-holder">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/<?php echo qtrans_getLanguage();?>/thewheel_en.png"
                             alt="How it works"/>
                    </div>
                    <div class="wheel-image">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wheel/<?php echo qtrans_getLanguage();?>/item1.png"
                             id="item1-image"
                             alt="How it works"/>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wheel/<?php echo qtrans_getLanguage();?>/item2.png"
                             id="item2-image"
                             alt="How it works"/>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wheel/<?php echo qtrans_getLanguage();?>/item3.png"
                             id="item3-image"
                             alt="How it works"/>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wheel/<?php echo qtrans_getLanguage();?>/item4.png"
                             id="item4-image"
                             alt="How it works"/>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/wheel/<?php echo qtrans_getLanguage();?>/item5.png"
                             id="item5-image"
                             alt="How it works"/>
                    </div>
                </div>
                <?php
                the_content();
                // End the loop.
                endwhile;
                ?>
            </div>
        </div>
        <div class="col-xs-3 right-page">
            <div class="box-sel-right">
                <?php include('our-location.php') ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
