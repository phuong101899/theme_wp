<?php
/*
Template Name: Template careers-detail
*/
get_header();
?>
<div class="container">
    <div class="wrap-job clearfix">
        <div class="sidebar-inc">
            <h4 class="head-side">Open Positions</h4>
            <?php
                 wp_nav_menu(array('theme_location' => 'job-menu', 'container_class' => 'location-job'));
            ?>
        </div>
        <div class="content-inc">
            <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile;
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>