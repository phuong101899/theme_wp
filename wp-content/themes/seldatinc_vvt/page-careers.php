<?php
/*
Template Name: Template careers
*/
get_header();
?>
<div class="container">
    <h3 class="head-new">
        <?php
            while (have_posts()) : the_post();
                the_title();
                // End the loop.
            endwhile;
            ?>
    </h3>
    <figure class="banner-job">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/new/banner-job.jpg" alt="">
    </figure>
    <div class="wrap-job clearfix">
        <div class="sidebar-inc">
            <h4 class="head-side">Open Positions</h4>
            <?php
                wp_nav_menu(array('theme_location' => 'job-menu', 'container_class' => 'location-job'));
            ?>
            
        </div>
        <div class="content-inc">
            <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile;
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>