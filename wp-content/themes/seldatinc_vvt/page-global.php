<?php
/*
Template Name: Template Global
*/
get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-xs-9 content-page contact-us">
            <div class="content">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_title('<h3 class="entry-title">', '</h3>');
                    the_content();
                    // End the loop.
                endwhile;
                ?>
            </div>
        </div>
        <div class="col-xs-3 right-page">
            <div class="box-sel-right">
                <?php include('our-location.php')?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
