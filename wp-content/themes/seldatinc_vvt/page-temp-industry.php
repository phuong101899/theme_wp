<?php
/*
Template Name: Your industry
*/

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-xs-3 left-page">
            <div class="box-sel">
                <h4 class="title">
				<?php 
					if (qtrans_getLanguage() == 'en')
						echo 'Logistic';
					elseif (qtrans_getLanguage() == 'vi')
						echo "Hậu Cần";
					elseif (qtrans_getLanguage() == 'es')
						echo 'Logistica';
				?>
				</h4>
                <?php
                wp_nav_menu(array('theme_location' => 'left-menu', 'container_class' => 'seldat_left_menu_class'));
                ?>
            </div>
        </div>
        <div class="col-xs-6 content-page">
            <div class="content">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_title('<h3 class="entry-title">', '</h3>');
                    the_content();
                    // End the loop.
                endwhile;
                ?>
            </div>
        </div>
        <div class="col-xs-3 right-page">
            <div class="box-sel-right">
                <?php include('our-location.php')?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
