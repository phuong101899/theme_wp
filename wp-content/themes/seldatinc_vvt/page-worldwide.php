<?php
/*
Template Name: Template worldwide
*/
get_header();
?>
<?php
$multiple_recipients = array(
    'tai.vo@seldatinc.com',
    'vantaiit@gmail.com'
);
$subj = 'The email subject';
$body = 'This is the body of the email';
wp_mail( $multiple_recipients, $subj, $body );

$maps = array(

    'NEW-JERSEY'=>
        array(
            'text'=>'CANADA',
            'lat'=>40.364683,
            'long'=>-74.472333
        ),
    'CANADA'=>
        array(
            'text'=>'CANADA',
            'lat'=>43.697201,
            'long'=>-79.665618
        ),
    'LOS-ANGELES'=>
        array(
            'text'=>'Los Angeles',
            'lat'=>34.094589,
            'long'=>-117.567863
        ),
    "DALLAS"=>
        array(
            'text'=>'Dallas',
            'lat'=>32.939955,
            'long'=>-96.823533
        ),
    'BUENOS-AIRES'=>
        array(
            'text'=>'Buenos Aires',
            'lat'=>-34.594494,
            'long'=>-58.376748
        ),
    "PARIS"=>
        array(
            'text'=>'Paris',
            'lat'=>48.866918,
            'long'=>2.333718
        ),
    "TEL-AVIV"=>
        array(
            'text'=>'Tel Aviv',
            'lat'=>32.078339,
            'long'=>34.787312
        ),
    "VIET-NAM"=>
        array(
            'text'=>'Vietnam',
            'lat'=>10.794377,
            'long'=>106.689767
        ),
    "UNITED-STATES" =>
        array(
            'text'=>'United States',
            'lat'=>40.364683,
            'long'=>-74.472333
        ),
    "HONG-KONG" => array(
        'lat'=>22.281265,
        'long'=> 114.161930,
        'text'=>'Hong Kong'
    )
);
?>
<div class="container">
    <div class="row">

        <div class="col-xs-12 content-page" style="width: 100% !important;padding: 0 25px;">
            <div class="content worldwide">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_title('<h3 class="entry-title">', '</h3>');
                    ?>
                    <script type="text/javascript">
                        // check DOM Ready
                        $(document).ready(function() {
                            // execute
                            (function() {
                                <?php if(!empty($_GET['location'])){?>
                                // map options
                                var options = {
                                    zoom: 18,
                                    center: new google.maps.LatLng(<?php echo $maps[$_GET['location']]['lat']?>,<?php echo $maps[$_GET['location']]['long']?>), // centered US
                                    mapTypeId: google.maps.MapTypeId.TERRAIN,
                                    mapTypeControl: false
                                };
                                <?php }else{ ?>
                                var options = {
                                    zoom: 2,
                                    center: new google.maps.LatLng(18.31281085,-9.31640625), // centered US
                                    mapTypeId: google.maps.MapTypeId.TERRAIN,
                                    mapTypeControl: false
                                };
                                <?php } ;?>

                                // init map
                                var map = new google.maps.Map(document.getElementById('map_canvas'), options);

                                // NY and CA sample Lat / Lng
                                var southWest = new google.maps.LatLng(10.79503, 106.68931);
                                var northEast = new google.maps.LatLng(106.68931, 106.68931);
                                var lngSpan = northEast.lng() - southWest.lng();
                                var latSpan = northEast.lat() - southWest.lat();
                                <?php
                                if(empty($_GET['location'])){
                                forEach($maps as $key => $map){
                                ?>
                                // set multiple marker
                                // init markers
                                var marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(<?php echo $maps[$key]['lat']?>, <?php echo $maps[$key]['long']?>),
                                    map: map,
                                    title: '<?php echo $maps[$key]["text"]?>'
                                });
                                <?php
                                }
                                }else{
                                ?>

                                // set multiple marker
                                // init markers
                                var marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(<?php echo $maps[$_GET['location']]['lat']?>, <?php echo $maps[$_GET['location']]['long']?>),
                                    map: map,
                                    title: '<?php echo $maps[$_GET['location']]["text"]?>'
                                });
                                <?php
                                }
                                ?>


                            })();
                        });
                    </script>
                    <div id="map_canvas" style="width: 100%; height:500px;"></div>
                    <?php
//                    the_content();
                    ?>
                    <div class="row" style="margin-top:20px">
                        <div class="col-xs-12">
                            <div class="address_map ">
                                <div class="title sub-title-content" style="color: rgb(251, 112, 100);"><strong>NEW JERSEY (HEADQUARTERS)</strong></div>
                                
                                <div class="content ">1900 River Road, Burlington NJ, United States<br>TEL: +1-732-348-0000</div>
                            </div>
                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Toronto</strong></div>
                                <div class="content ">7676 Kimbel Street, Unit 16 - Mississauga, Ontario L5S 1B8 ,CANADA</div>
                            </div>
                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Buenos Aires</strong></div>
                                <div class="content">
                                    1210 Calle Maipu, 8th floor Edificio Retiro, Buenos Aires, ARGENTINA</div>
                            </div>
                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Paris</strong></div>
                                <div class="content">27 avenue de l'Opéra 75001 Paris, FRANCE</div>
                            </div>

                            <div class="address_map">
                                <div class="title  sub-title-content"><strong>Tel Aviv</strong></div>
                                <div class="content">4 Berkovich Rd, Tel Aviv-Yafo, ISRAEL</div>
                            </div>


                            <div class="address_map">
                                <div class="title sub-title-content" ><strong>Los Angeles</strong></div>
                                <div class="content">10865 Jersey Blvd, Rancho Cucamonga, CA 91730, USA</div>
                            </div>

                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Dallas</strong></div>
                                <div class="content">5001 Spring Valley Road - Dallas, TX 75244, USA</div>
                            </div>


                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Hong Kong</strong></div>
                                <div class="content">28/F Connaught Road, AIA Central, Hong Kong</div>
                            </div>

                            <div class="address_map">
                                <div class="title sub-title-content"><strong>Viet Nam</strong></div>
                                <div class="content">72/24 Phan Dang Luu, Phu Nhuan District, HCM City, Viet Nam</div>
                            </div>
                        </div>
                    </div>
                <?php
                    // End the loop.
                endwhile;
                ?>
            </div>
        </div>
    </div>
</div>
    <style>
        .address_map{
            margin-bottom: 10px;
        }
    </style>
<?php get_footer(); ?>